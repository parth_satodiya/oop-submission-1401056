#include<iostream>

using namespace std;

class Bank					//create class'Bank'
{

	private:
	
	int i;

	//Bank  
	char branch_city[20];
	char branch_name[20];

	//Personal
	char customer_name[25];
	char customer_address[50];
	char customer_phone[12];

	//Account
	int account_no;
	int balance;

	public:
	Bank(){}					//Default Constructor

	Bank(int ac)				//Constructor with parameter
	{
		account_no=ac;			//accept account no. from main function
	}
	
	void getDetails();			//to create new account
	void printDetails();		// to print account details
	void updateDetails();		// to update account details
	void credit();				//to enter credit details
	void debit();				//to enter debit details

	int getAccountaNo()
	{
		return account_no;		// function that return account no.
	}

};




void Bank::getDetails()
{
	cout<<endl<<"Enter Customer Name : ";
	cin>>customer_name;
	
	cout<<endl<<"Enter Customer address : ";
	cin>>customer_address;

	cout<<endl<<"Enter Customer phone number : ";
	cin>>customer_phone;

	cout<<endl<<"Enter account branch city : ";
	cin>>branch_city;

	cout<<endl<<"Enter branch name : ";
	cin>>branch_name;

	cout<<endl<<"Howmuch rupees you want to keep in this account : ";
	cin>>balance;

	cout<<endl<<"Your Account No. is : "<<account_no;	
}

void Bank::printDetails()
{
	
	cout<<endl<<"Customer Name : "<<customer_name;
	cout<<endl<<"Customer Address : "<<customer_address;
	cout<<endl<<"Customer Phone no. : "<<customer_phone;
	cout<<endl<<"Branch City : "<<branch_city;
	cout<<endl<<"Branch Name : "<<branch_name;
	cout<<endl<<"Customer Account Balance : "<<balance;
}

void Bank::updateDetails()
{
	cout<<endl<<"Enter Customer address : ";
	cin>>customer_address;

	cout<<endl<<"Enter Customer phone number : ";
	cin>>customer_phone;

	cout<<endl<<"Enter account branch city : ";
	cin>>branch_city;

	cout<<endl<<"Enter branch name : ";
	cin>>branch_name;
}

void Bank::credit()
{
	int m;

	cout<<"Enter total ammount you credited : ";
	cin>>m;
	
	balance=balance + m;
	cout<<"Now, your balance is "<<balance;
}

void Bank::debit()
{
	int m;

	cout<<endl<<"Enter total ammount you debited : ";
	cin>>m;
	
	balance=balance - m;						
	cout<<"Now, your balance is "<<balance;
}
