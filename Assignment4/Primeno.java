import java.util.Scanner;

class Primeno
{
	public static void main(String arg[])
	{
		int a,b,i,j;
		Scanner S=new Scanner(System.in);	
	
		System.out.println("Enter your starting range : ");		//Take Starting Range
		a=S.nextInt();

		System.out.println("Enter your ending range : ");		//Take ending range
		b=S.nextInt();
	
		for(i=a;i<=b;i++)
		{	
			for(j=2;j<i;j++)
			{
				if(i%j==0)
				{
					System.out.println(i + " is not a prime number. \n");		//Print if no. is not prime
					break;
				}

				else if(j==(i-1) && i%j!=0)
				{
					System.out.println(i + " is a prime number. \n");		//Print if no. is prime
					break;
				}
			}
		}

	}
}
