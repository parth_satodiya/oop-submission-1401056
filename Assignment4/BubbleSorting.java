import java.util.Scanner;

class BubbleSorting
{
	public static void main(String arg[])
	{
		int a,b,k,i,j;

		Scanner S=new Scanner(System.in);
		System.out.println("Howmany numbers you want to enter : ");				//Enter hoemany no. you want to sort.
		
		k=S.nextInt();
		int n[]=new int[k];

		for(i=0;i<k;i++)
		{
			System.out.println("Enter your Number : ");					//Enter all no. you want to sort.
			n[i]=S.nextInt();
		}

		for(i=0;i<k-1;i++)
		{
			for(j=i+1;j<k;j++)
			{
				if(n[i]>n[j])
				{
					int t=n[j];							//Swap no. if the next no. is smaller
                    			n[j]=n[i];
                   			n[i]=t;
				}
			}
		}
        
        System.out.println("your Numbers after sorting  : ");

        for(i=0;i<k;i++)
		{
			System.out.print(n[i] + " ");							//Print sorting no.
		}

	}
}
