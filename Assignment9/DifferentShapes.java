import java.util.Scanner;
import java.io.*;		//import file library
import java.lang.*;		//import math library

abstract class Shape		//shape abstract class
{
	private String color;
	
	public Shape()		//constructor
	{
		color = null;
	}
	
	public void setColor(String c)
	{
		color = c;
	}
	
	public String getColor()
	{
		return color;
	}
}

abstract class TwoD extends Shape
{		
	public double findRArea(double[] A) 
	{				
		double temp_l = ( (A[0]-A[2])*(A[0]-A[2]) + (A[1]-A[3])*(A[1]-A[3]) ) ;
		double temp_h = ( (A[0]-A[4])*(A[0]-A[4]) + (A[1]-A[5])*(A[1]-A[5]) );
		
		double length = Math.sqrt(temp_l);
		double height = Math.sqrt(temp_h);
		
		System.out.println("Length : " + length);
		System.out.println("Height : " + height);
		
		return (length*height);
	}
	
	public double findCArea(double[] A)
	{
		double temp = (A[0]-A[2])*(A[0]-A[2]) + (A[1]-A[3])*(A[1]-A[3]);	//distance formula
		double radius = Math.sqrt(temp);					//take square root
		
		System.out.println("Radius : " + radius);
		
		return (22/7)*radius*radius; 
	}
}

abstract class ThreeD extends Shape
{
		
	public double findCuVolume(double[] A) 
	{			
		double temp_l = (A[0]-A[2])*(A[0]-A[2]) + (A[1]-A[3])*(A[1]-A[3]);	//distance formula
		double temp_h = (A[0]-A[4])*(A[0]-A[4]) + (A[1]-A[5])*(A[1]-A[5]);
		double temp_w = (A[0]-A[6])*(A[0]-A[6]) + (A[1]-A[7])*(A[1]-A[7]);
		
		double length = Math.sqrt(temp_l);
		double height = Math.sqrt(temp_h);
		double width = Math.sqrt(temp_w);
		
		System.out.println("Length :" + length);
		System.out.println("Height :" + height);
		System.out.println("Width :" + width);
		
		return (length*height*width);			//return volume
	}
	
	public double findCVolume(double[] A)
	{
		double temp = (A[0]-A[2])*(A[0]-A[2]) + (A[1]-A[3])*(A[1]-A[3]);
		double redius = Math.sqrt(temp);
		
		System.out.println("Radius :" + redius);
		
		return (4/3)*(22/7)*redius*redius*redius; 
	}
}

class Rectangle extends TwoD implements Serializable		//class for rectangle
{
	double[] vert = new double[6];
	
	Rectangle()
	{
		for(int i=0; i<=5; i++)			//initialization
		{
			vert[i] = 0;
		}	
	}
	
	public void set_vertices(double lx, double ly, double refx, double refy, double hx, double hy)
	{
		vert[0] = refx;	vert[1] = refy;
		vert[2] = lx;	vert[3] = ly;
		vert[4] = hx;	vert[5] = hy;
	}
}

class Circle extends TwoD implements Serializable	//class for circle
{
	double[] vert = new double[4];
	
	Circle()
	{
		for(int i=0; i<=3; i++)
		{
			vert[i] = 0;
		}	
	}
	
	public void set_vertices(double cenx, double ceny, double radx, double rady)
	{
		vert[0] = cenx;		vert[1] = ceny;
		vert[2] = radx;		vert[3] = rady;
	}
}

class Cuboid extends ThreeD implements Serializable	//Cubiod class
{
	double[] vert = new double[8];		//take 8 bit array   for store three points to calculate VOLUME
	
	Cuboid()
	{
		for(int i=0; i<=7; i++)
		{
			vert[i] = 0;
		}	
	}
	
	public void set_vertices(double lx, double ly, double refx, double refy, double hx, double hy, double wx, double wy)
	{
		vert[0] = refx;	vert[1] = refy;
		vert[2] = lx;	vert[3] = ly;
		vert[4] = hx;	vert[5] = hy;
		vert[6] = wx;   vert[7] = wy;
	}
}

class Sphere extends ThreeD implements Serializable	//sphere class
{
	public double[] vert = new double[4];
	
	Sphere()
	{
		for(int i=0; i<=3; i++)
		{
			vert[i] = 0;
		}	
	}
	
	public void set_vertices(double cenx, double ceny, double radx, double rady)
	{
		vert[0] = cenx;	vert[1] = ceny;
		vert[2] = radx;	vert[3] = rady;
	}
	
}

class DifferentShapes implements Serializable		//main class
{
	public static void main(String argv[])
	{
		byte choice1, choice2, choice3;
	
		TwoD td;
		ThreeD trd;

		Scanner sc = new Scanner(System.in);
	
		Rectangle R = new Rectangle();
		Circle Ci = new Circle();
		Cuboid Cu = new Cuboid();
		Sphere S = new Sphere();

		do{
		System.out.println("Press 1 --> Enter Data : ");
		System.out.println("Press 2 --> Show Data : ");
		System.out.println("Press 3 --> Delete Data : ");
		System.out.println("Press 4 --> Exit : ");
		choice1 = sc.nextByte();
	
		switch(choice1)
		{

			case 1:
				System.out.println("Press 1 --> Two dimension shape : ");
				System.out.println("Press 2 --> Three dimension shape : ");
				choice2 = sc.nextByte();
		
				if(choice2 == 1)
				{
					System.out.println("Press 1 --> Rectangle : ");
					System.out.println("Press 2 --> Circle : ");
					choice3 = sc.nextByte();
				
					if(choice3 == 1)
					{	
						System.out.println("Enter filename : ");
						String filename = sc.next(); 
						
						System.out.println("Enter Rectangle's color :");
						String color = sc.next();
						
						System.out.println("Enter Reference point's x :");
						double refx = sc.nextDouble();
					
						System.out.println("Enter Reference point's y :");
						double refy = sc.nextDouble();
					
						System.out.println("Enter length point's x :");
						double lx = sc.nextDouble();
					
						System.out.println("Enter length point's y :");
						double ly = sc.nextDouble();
					
						System.out.println("Enter height point's x :");
						double hx = sc.nextDouble();
					
						System.out.println("Enter height point's y :");
						double hy = sc.nextDouble();
					
						R.setColor(color);				//set color
						R.set_vertices(lx, ly, refx, refy, hx, hy);
						
						System.out.println("Rectangle's color : " + R.getColor());
						System.out.println("Rectangle's area is : " + R.findRArea(R.vert) + "\n");
						
						FileOutputStream fos = null;
						ObjectOutputStream oos = null;
						
						try{
							fos = new FileOutputStream(filename);
							oos = new ObjectOutputStream(fos);
							
							oos.writeObject(R);		//write object in file
						}
						
						catch(Exception e)
						{
							System.out.println("Error : " + e);
						}
						
						finally
						{
							try
							{
								oos.close();		//finally close file
							}
							catch(Exception e)
							{
								System.out.println("Error : " + e);
							}
						}
					}
					else
					{
						System.out.println("Enter filename : ");
						String filename = sc.next(); 
						
						System.out.println("Enter circle's color :");
						String color = sc.next();
					
						System.out.println("Enter center point's x :");
						double cenx = sc.nextDouble();
					
						System.out.println("Enter center point's y :");
						double ceny = sc.nextDouble();
					
						System.out.println("Enter radius point's x :");
						double radx = sc.nextDouble();
					
						System.out.println("Enter radius point's y :");
						double rady = sc.nextDouble();
					
						Ci.set_vertices(cenx, ceny, radx, rady);
						
						Ci.setColor(color);		//set color
						System.out.println("Rectangle's color : " + Ci.getColor());
						System.out.println("Circle's area is : " + Ci.findCArea(Ci.vert) + "\n");
						
						FileOutputStream fos = null;
						ObjectOutputStream oos = null;
						
						try{
							fos = new FileOutputStream(filename);
							oos = new ObjectOutputStream(fos);
							
							oos.writeObject(Ci);		//write object in file
						}
						
						catch(Exception e)
						{
							System.out.println("Error : " + e);
						}
						
						finally
						{
							try
							{
								oos.close();
							}
							catch(Exception e)
							{
								System.out.println("Error : " + e);
							}
						}
					}
				}
			
				else
				{
					System.out.println("Press 1 --> Cuboid : ");
					System.out.println("Press 2 --> Sphere : ");
					choice3 = sc.nextByte();
					
					if(choice3 == 1)
					{	
						System.out.println("Enter filename : ");
						String filename = sc.next(); 
				
						System.out.println("Enter Cuboid's color :");
						String color = sc.next();
				
						System.out.println("Enter Reference point's x :");
						double refx = sc.nextDouble();
					
						System.out.println("Enter Reference point's y :");
						double refy = sc.nextDouble();
					
						System.out.println("Enter length point's x :");
						double lx = sc.nextDouble();
					
						System.out.println("Enter length point's y :");
						double ly = sc.nextDouble();
					
						System.out.println("Enter height point's x :");
						double hx = sc.nextDouble();
					
						System.out.println("Enter height point's y :");
						double hy = sc.nextDouble();
						
						System.out.println("Enter width point's x :");
						double wx = sc.nextDouble();
					
						System.out.println("Enter width point's y :");
						double wy = sc.nextDouble();
					
						Cu.set_vertices(lx, ly, refx, refy, hx, hy, wx, wy);
						
						Cu.setColor(color);		//set color
						System.out.println("Rectangle's color : " + Cu.getColor());
						System.out.println("Cube's volume is : " + Cu.findCuVolume(Cu.vert) + "\n");
						
						FileOutputStream fos = null;
						ObjectOutputStream oos = null;
						
						try{
							fos = new FileOutputStream(filename);
							oos = new ObjectOutputStream(fos);
							
							oos.writeObject(Cu);		//write object in file
						}
						
						catch(Exception e)
						{
							System.out.println("Error : " + e);
						}
						
						finally
						{
							try
							{
								oos.close();
							}
							catch(Exception e)
							{
								System.out.println("Error : " + e);
							}
						}
					}
					else
					{
						System.out.println("Enter filename : ");
						String filename = sc.next(); 
						
						System.out.println("Enter Rectangle's color :");
						String color = sc.next();
					
						System.out.println("Enter center point's x :");
						double cenx = sc.nextDouble();
					
						System.out.println("Enter center point's y :");
						double ceny = sc.nextDouble();
					
						System.out.println("Enter radius point's x :");
						double radx = sc.nextDouble();
					
						System.out.println("Enter radius point's y :");
						double rady = sc.nextDouble();
					
						S.set_vertices(cenx, ceny, radx, rady);
						
						S.setColor(color);		//set color
						System.out.println("Rectangle's color : " + S.getColor());
						System.out.println("Sphere's volume is : " + S.findCVolume(S.vert)+ "\n");
						
						FileOutputStream fos = null;
						ObjectOutputStream oos = null;
						
						try{
							fos = new FileOutputStream(filename);
							oos = new ObjectOutputStream(fos);
							
							oos.writeObject(S);		//write object in file
						}
						
						catch(Exception e)
						{
							System.out.println("Error : " + e);
						}
						
						finally
						{
							try
							{
								oos.close();
							}
							catch(Exception e)
							{
								System.out.println("Error : " + e);
							}
						}
					}
				}
				break;
		
			case 2:
				System.out.println("Enter filename : ");
				String filename = sc.next();
				
				File f = new File(filename);
				
				if(f.isFile())
				{
				
					System.out.println("Press 1 --> Two dimension shape : ");
					System.out.println("Press 2 --> Three dimension shape : ");
					choice2 = sc.nextByte();
		
					if(choice2 == 1)
					{
						System.out.println("Press 1 --> Rectangle : ");
						System.out.println("Press 2 --> Circle : ");
						choice3 = sc.nextByte();
				
						if(choice3 == 1)
						{	
							Rectangle RTemp = new Rectangle();
							FileInputStream fis = null;
							ObjectInputStream ois = null;
						
							try{
								fis = new FileInputStream(filename);
								ois = new ObjectInputStream(fis);
							
								RTemp = (Rectangle)ois.readObject();	//read stored object
								
								System.out.println("Rectangle's color : " + RTemp.getColor());
								System.out.println("Rectangle's area is : " + RTemp.findRArea(RTemp.vert) + "\n");
							}
						
							catch(Exception e)
							{
								System.out.println("Error : " + e);
							}
						
							finally
							{
								try
								{
									ois.close();
								}
								catch(Exception e)
								{
									System.out.println("Error : " + e);
								}
							}
						}
						else
						{
							Circle CTemp = new Circle();
							FileInputStream fis = null;
							ObjectInputStream ois = null;
						
							try{
								fis = new FileInputStream(filename);
								ois = new ObjectInputStream(fis);
							
								CTemp = (Circle)ois.readObject();	//read stored object
								
								System.out.println("Rectangle's color : " + CTemp.getColor());
								System.out.println("Circle's area is : " + CTemp.findCArea(CTemp.vert) + "\n");
							}
						
							catch(Exception e)
							{
								System.out.println("Error : " + e);
							}
						
							finally
							{
								try
								{
									ois.close();
								}
								catch(Exception e)
								{
									System.out.println("Error : " + e);
								}
							}
						}
					}
			
					else
					{
						System.out.println("Press 1 --> Cuboid : ");
						System.out.println("Press 2 --> Sphere : ");
						choice3 = sc.nextByte();
					
						if(choice3 == 1)
						{	
							Cuboid CuTemp = new Cuboid(); 
							FileInputStream fis = null;
							ObjectInputStream ois = null;
						
							try{
								fis = new FileInputStream(filename);
								ois = new ObjectInputStream(fis);
							
								CuTemp = (Cuboid)ois.readObject();	//read stored object
								
								System.out.println("Rectangle's color : " + CuTemp.getColor());
								System.out.println("Cube's volume is : " + CuTemp.findCuVolume(CuTemp.vert) + "\n");
							}
						
							catch(Exception e)
							{
								System.out.println("Error : " + e);
							}
						
							finally
							{
								try
								{
									ois.close();
								}
								catch(Exception e)
								{
									System.out.println("Error : " + e);
								}
							}
						}
						else
						{	
							Sphere STemp = new Sphere();
							FileInputStream fis = null;
							ObjectInputStream ois = null;
						
							try{
								fis = new FileInputStream(filename);
								ois = new ObjectInputStream(fis);
							
								STemp = (Sphere)ois.readObject();	//read stored object
								
								System.out.println("Rectangle's color : " + STemp.getColor());
								System.out.println("Sphere's volume is : " + STemp.findCVolume(STemp.vert)+ "\n");
							}
						
							catch(Exception e)
							{
								System.out.println("Error : " + e);
							}
						
							finally
							{
								try
								{
									ois.close();
								}
								catch(Exception e)
								{
									System.out.println("Error : " + e);
								}
							}
						}
					}	
				}
				
				else
				{
					System.out.println("File doesn't exist!\n");
				}
				break;
			
			case 3:
				System.out.println("Enter filename : ");
				filename = sc.next();
				
				f = new File(filename);		//create object of file
				
				if(f.isFile())
				{
					f.delete();		//if file exist then delete it
					System.out.println("File detected! \n");
				}
				
				else
				{
					System.out.println("File doesn't exist! \n");
				}
		}
		}while(choice1>0 && choice1<4);
	}
}