import java.util.Scanner;
import java.io.*;					//import file library

class Credit_Withdraw_Exception extends Exception	//create my exception	//inherite Exception class
{	
	private float check;
	public Credit_Withdraw_Exception()
	{}
	
	public String toString()
	{
		return "Value must be > 0 ";			//return string
	} 
}

abstract class Account			//abstract account class
{
	protected static double account_no = 1331;
	protected float balance;
	
	public Account(float bal)		//Parent class's constrator
	{
		balance = bal;
	}
	
	public void credit(float r) throws Credit_Withdraw_Exception			//common credit function 
	{
		Credit_Withdraw_Exception C = new Credit_Withdraw_Exception();
		if(r<0)
		{
			throw C;
		}
		balance += r;
	}
	
	public float getBalance()			//common get balance function
	{
		return balance;
	
	}
	
	public abstract double getAccountNo();			
	
	public abstract void deleteAccount();
	
	public abstract void createFile();		
	
	public abstract void withdraw(float withdraw)throws Credit_Withdraw_Exception;	//abstract function withdraw because it is necessary for both child class 
}

class Savings extends Account				//child class of Account class		(for savings account)
{
	Scanner sc = new Scanner(System.in);
	
	private float min_balance = 500;					//must remain 500 rupees every time in your account
	private float interest_rate, interest, total_interest=0, temp1=1, temp2;
	private int i, current_month, current_year, month, year, total_month = 3;
	private int quarter;
	private double s_account_no;
	private String s_string_account_no;
		
	public Savings(float interest, float bal)		//constructor for saving account
	{
		super(bal);					//call parent class constructor
		interest_rate = interest;
		account_no++;
		s_account_no = account_no;
		s_string_account_no = String.valueOf(s_account_no);			//common get account number function
	}	
		
	public String getStringAccountNo()			//common get account number function
	{
		return s_string_account_no;
	}
		
	public void setCurrentDate(int cm, int cy)			//function for set today's date
	{
		current_month = cm; 		
		current_year = cy;
	}
	
	public double getAccountNo()			//common get account number function
	{
		return s_account_no;
	}
	
	
	public boolean setAccountDate()					//Enter date when you created account
	{
		System.out.println("Enter account month : ");	
		month = sc.nextInt(); 	
		
		System.out.println("Enter account year : ");	
		year = sc.nextInt();
		
		if(month <= 0 || year <= 0 || month > 12)		//check valid date 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void interest()						//function for calculate interest quataraly
	{
		if(year == current_year && month>current_month)		//account date is less then today's date
		{
			System.out.println("Account date is Incorrect!");
			return;
		}
		else if(year>current_year)				//account date is less then today's date
		{
			System.out.println("Account date is Incorrect!");
			return;
		}
		
		else if(total_month < 3)				//total_month check the difference between today's and account's date
		{
			System.out.println("You already calculate interest for this account!");
			return;
		}
		
		else
		{
			total_month = ((12 *( current_year - year)) + current_month) - month;		
			
			/* calculate total month difference between today's date and account's date*/
													
			quarter = (total_month/3);		//calculate quarter
		
			for(i=1; i<=quarter; i++)
			{
				interest = (balance * interest_rate * ((float)1/4)) / 100;      //calculate simple interest for each quarter
				System.out.println(i + " quarter interest is : " + interest);
				balance = balance + interest;
				total_interest = total_interest + interest;
			}
					
			System.out.println("Your interest is : "+ total_interest);
			System.out.println("Your final balance is : " + balance+ "\n");
			
			total_month = total_month - (3 * quarter);
			
			System.out.println("Your next interest will applied after "+ (3 - total_month) +" month/s \n");
		}
	}
	
	public void setMinBalance(float set_minimum_balance)		//set min balance which is min for saving account. less then this is not allowed
	{
		min_balance = set_minimum_balance;		
	}
	
	public float showMinBalance()					//show main balance
	{
		return min_balance;
	}
	
	public float showInterestRate()					//show interest rate for saving account
	{
		return interest_rate;
	}
	
	public void setInterestRate(float set_rate)			//set interest rate 
	{
		interest_rate = set_rate;		
	}
	
	public void showAccount()					//show saving account
	{
		System.out.println("Account No. : " + getAccountNo());
		System.out.println("Min balance for saving accounts : " + showMinBalance());
		System.out.println("Balance : " + getBalance());
		System.out.println("Interest rate : "+ showInterestRate());
	}
	
	public void withdraw(float withdraw)throws Credit_Withdraw_Exception			//withdraw function for saving account
	{
		Credit_Withdraw_Exception C = new Credit_Withdraw_Exception();
	
		if(withdraw < 0)
		{
			throw C;
		}
		
		else if(withdraw > (balance-min_balance) )			//check withdraw amount
		{			
			System.out.println("Insufficient balance ( balance must remain greater then 500 for all time. )");
		}
		
		else
		{
			balance -= withdraw;
		}
	}
	
	public void createFile()			//function for create file for saving account
	{					
		String fileName = getStringAccountNo();	
		FileOutputStream outs = null;				
		
		try{					
			outs = new FileOutputStream(fileName);

			String type = "This is Saving Account.\n";
			String ac = "Account No. : "+ getAccountNo()+"\n";
			String ra = "Interest Rate :" + interest_rate + "\n";
			String ba = "Balance :"+ getBalance();
			
			byte[] t = type.getBytes();
			byte[] a = ac.getBytes();
			byte[] r = ra.getBytes();
			byte[] b = ba.getBytes();
			
			outs.write(t);
			outs.write(a);
			outs.write(r);
			outs.write(b);												
			
		}
		catch(IOException e) 			//check exception
		{
		    System.out.println("Error writing to file '"+ fileName + "'");
		    System.out.println("Error : " + e + "\n");
		}
		
		finally
		{
			try{
				outs.close();			//close file
			}
			catch(IOException e) 
			{
			    System.out.println("Error : " + e + "\n");
			}
			
		}
	}
	
	public void deleteAccount()
	{
		File file = new File(getStringAccountNo());
		
		if(file.delete())		//delete file
		{
			System.out.println("Account deleted successful\n");	
		}
		else
		{
			System.out.println("delete operation unsuccessful\n");
		}
	}
}

class Current extends Account				//child class of account class    (for currennt account)
{
	private long[] overdraft_limit = new long[]{500, 5000, 50000, 500000, 5000000, 10000000};

	private long income;
	private double c_account_no;
	private String c_string_account_no;
	
	Current(long in, float bal)		//constructor for current
	{
		super(bal);				//parent class's constructor
		income = in;
		account_no++;
		c_account_no = account_no;
		c_string_account_no = String.valueOf(c_account_no);
	}
	
	public double getAccountNo()			//common get account number function
	{
		return c_account_no;
	}
	
	
	public String getStringAccountNo()			//common get account number function
	{
		return c_string_account_no;
	}
	
	public long showIncome()				//show income
	{
		return income;
	}
	
	public void showAccount()				//show current account
	{
		System.out.println("Account No. : "+getAccountNo());
		System.out.println("Account Balance : "+getBalance());
		System.out.print("Account Overdraft Limit : ");
		showOverdraftLimit();
	}
	
	public void setOverdraftLimit(long over_limit)		//set overdraft limit for your current account (income wise)
	{
		if(income < 6000)
		{
			overdraft_limit[0] = over_limit;
		}	
		
		if(income < 60000)
		{
			overdraft_limit[1] = over_limit;
		}	
		
		if(income < 800000)
		{
			overdraft_limit[2] = over_limit;
		}	
		
		if(income < 2000000)
		{
			overdraft_limit[3] = over_limit;
		}	
		
		if(income < 30000000)
		{
			overdraft_limit[4] = over_limit;
		}	
		
		if(income > 30000000)
		{
			overdraft_limit[5] = over_limit;
		}	
	}
	
	public long showOverdraftLimit()			//show overdraft limit for current account (income wise)
	{
		if(income < 6000)
		{
			return overdraft_limit[0];
		}
		
		else if(income < 60000)
		{
			return overdraft_limit[1];
		}
		
		else if(income < 800000)
		{
			return overdraft_limit[2];
		}
		
		else if(income < 2000000)
		{
			return overdraft_limit[3];
		}
		
		else if(income < 30000000)
		{
			return overdraft_limit[4];
		}
		
		else if(income > 30000000)
		{
			return overdraft_limit[5];
		}
		else
		return 0;
	}		
	
	
	public void withdraw(float withdraw) throws Credit_Withdraw_Exception		//withdraw function for current account 
	{
		Credit_Withdraw_Exception C = new Credit_Withdraw_Exception();
	
		if(withdraw < 0)
		{
			throw C;			//throws exception if amount is < 0
		}
		
		if(income < 6000)
		{
			if(withdraw > ( balance + overdraft_limit[0]))
			System.out.println(" Insufficient balance! (Yor cross the overdraft limit "+ overdraft_limit[0] +")");
						
			else
			balance -= withdraw; 
		}
		
		else if(income < 60000)
		{
			if(withdraw > ( balance + overdraft_limit[1]))
			System.out.println(" Insufficient balance! (Yor cross the overdraft limit 5000 " +overdraft_limit[1] +")");
			
			else
			balance -= withdraw; 
		}
		
		else if(income < 800000)
		{
			if(withdraw > ( balance + overdraft_limit[2]))
			System.out.println(" Insufficient balance! (Yor cross the overdraft limit 50000 " + overdraft_limit[2]+ ")");
			
			else
			balance -= withdraw; 
		}
		
		else if(income < 2000000)
		{
		
		
			if(withdraw > ( balance + overdraft_limit[3]))
			System.out.println(" Insufficient balance! (Yor cross the overdraft limit 500000 " + overdraft_limit[3]+ ")");
			
			else
			balance -= withdraw; 
		}
		
		else if(income < 30000000)
		{
			if(withdraw > ( balance + overdraft_limit[4]))
			System.out.println(" Insufficient balance! (Yor cross the overdraft limit 5000000 " + overdraft_limit[4]+ ")");
			
			else
			balance -= withdraw; 
		}
		
		else if(income > 30000000)
		{
			if(withdraw > ( balance + overdraft_limit[5]))
			System.out.println(" Insufficient balance! (Yor cross the overdraft limit 10000000 " +overdraft_limit[5] + ")");
			
			else
			balance -= withdraw; 
		}
	}
	
	public void createFile()		//createfile member function for current account
	{
		String fileName = getStringAccountNo();	
		FileOutputStream outs = null;		
					
		try{		
			outs = new FileOutputStream(fileName);

			String type = "This is Current Acount. \n";		//take string value
			String ac = "Account No. : "+ getAccountNo()+"\n";
			String inc = "Income :"+ income + "\n";
			String ba = "Balance :"+ getBalance()+ "\n";
			String ov = "Overdraft Limit :" + showOverdraftLimit();
			
			byte[] t = type.getBytes();				//convert in byte
			byte[] a = ac.getBytes();
			byte[] ic = inc.getBytes();
			byte[] b = ba.getBytes();
			byte[] o = ov.getBytes();
			
			outs.write(t);						//write in file
			outs.write(a);
			outs.write(ic);
			outs.write(b);
			outs.write(o);												

			outs.close();
		}
		catch(IOException e) 
		{
		    System.out.println("Error writing to file '"+ fileName + "'");
		    System.out.println("Error : " + e + "\n");
		}
		
		finally
		{
			try{
				outs.close();			//finally close the file
			}
			catch(IOException e) 
			{
			    System.out.println("Error : " + e + "\n");
			}
			
		}
	}
	
	public void deleteAccount()			//delete file
	{
		File file = new File(getStringAccountNo());
		
		if(file.isFile())
		{
			try
			{
				if(file.delete())
				{
					System.out.println("Account deleted successful\n");	
				}
				else
				{
					System.out.println("delete operation unsuccessful\n");
				}
		
			}
		
			catch(Exception e) 
			{
			    	System.out.println("delete operation unsuccessful\n");
			}
		}
		else
		{
			System.out.println("File Doesn't Found!\n");
		}
	}
}

class FileBanking_E						//main banking class
{
	public static void main(String argv[])
	{
		Scanner sc = new Scanner(System.in);
		
		int i, sj=0, cj=0, s, c, current_month, current_year;
		byte option, o1, o2;
		double ac_no;
		float rate;
		float min_balance, bal, credit, withdraw;		
		long in,limit;
				
		System.out.println("Howmany Savings accounts you want to create? : ");
		s = sc.nextInt();
		Savings[] S = new Savings[s];					//saving object array
		
		
		System.out.println("Howmany Current accounts you want to create? : ");
		c = sc.nextInt();
		Current[] C = new Current[c];					//current object array
		
		do{
		System.out.println("______________________________________________________________");		
		System.out.println("Press 1 : Add savings account's details : ");
		System.out.println("Press 2 : Add current account details : ");
		System.out.println("Press 3 : Set Minimum balance for savings account : ");
		System.out.println("Press 4 : Set Interest rate for savings account : ");
		System.out.println("Press 5 : Set Overdraft limit for current account : ");
		System.out.println("Press 6 : Credit/Withdraw : ");
		System.out.println("Press 7 : View accounts : ");
		System.out.println("Press 8 : View interest with final balance : ");
		System.out.println("Press 9 : Delete Account : ");
		System.out.println("Press 0 : Exit : ");
		option = sc.nextByte();
		
		switch(option)		
		{
			case 1:				//Add saving account Details
			if(s==0)
			{
				System.out.println("No saving account created!");
				break;
			}
			
			else if(sj==s)
			{
				System.out.println("You can not enter more saving account!");
				break;
			}
			
			else
			{
				for(i=0; i<s; i++)
				{					
					
					System.out.println("Enter savings interest rate for this account (rate between 0 and 20): ");
					rate = sc.nextFloat();		
					
					if(rate < 0.1 || rate > 20)
					{
						System.out.println("Entered incorrect details!");
						break;
					}
						
					do{
						System.out.println("Enter inialtial balance (must be greaterthen 1000) : ");
						bal = sc.nextFloat();
					}while(bal < 1000);
	
					System.out.println("Savings account created!");
	
					S[i] = new Savings(rate, bal);			//create saving class's object
					
					if(S[i].setAccountDate())				//call boolean function fro set account date
					{
						System.out.println("Entered incorrect details!");
						break;
					}
					
					System.out.println("Your account no. is : " + S[i].getAccountNo());
					
					
        				
        				S[i].createFile();
					
					sj++;
					System.out.println("__________________________________________________\n");

				}
					System.out.println("Enter current month : ");	
					current_month = sc.nextInt(); 	
		
					System.out.println("Enter current year : ");	
					current_year = sc.nextInt();
					
					if(current_month < 1 || current_year < 1 || current_month > 12)	//check validity for date
					{
						System.out.println("Entered incorrect details!");
						break;
					}
					for(i=0;i<s;i++)				//set today's date for all saving account's object
					{
						S[i].setCurrentDate(current_month, current_year);					
					}
					
				break;
			}
			
			case 2:				//Add current account details
			if(c==0)
			{
				break;
			}
			
			else if(cj==c)
			{
				System.out.println("You can not enter more current account!");
				break;
			}
			
			else
			{
				for(i=0; i<c; i++)
				{
					System.out.println("Enter your income : ");
					in = sc.nextLong();
	
					System.out.println("Enter initial balance : ");
					bal = sc.nextFloat();
					
					if(in < 1 ||  bal < 1)
					{
						System.out.println("Entered incorrect details!");
						break;
					}
	
					System.out.println("Current account created!");
	
					C[i] = new Current(in, bal);		//create current class's object
					
					System.out.println("Your account no. is : " + C[i].getAccountNo());
					
					
        				
        				C[i].createFile();
					
					cj++;
					System.out.println("__________________________________________________\n");			
				}
			}
			break;
			
			case 3:				//set minimum balance for saving account
			
			if(sj == 0)		//check whether account details entered or not
			{
				System.out.println("No account crated!");
				break;
			}
			else
			{
				System.out.println("Enter Savings Account no.");
				ac_no = sc.nextDouble();
			
				for(i=0; i<s; i++)
				{
					if(ac_no == S[i].getAccountNo())
					{
						System.out.println("Current minimum balance is : " + S[i].showMinBalance());
			
						System.out.println("Enter minimun balance : ");
						min_balance = sc.nextFloat();
			
						S[i].setMinBalance(min_balance);	
						break;				
					}
				}
				break;
			}
			
			case 4:			//set interest rate for saving account
			
			if(sj == 0)		//check whether account details entered or not
			{
				System.out.println("No account crated!");
				break;
			}
			
			else{
				System.out.println("Enter Savings Account no.");
				ac_no = sc.nextDouble();
			
				for(i=0; i<s; i++)
				{
					if(ac_no == S[i].getAccountNo())
					{
						System.out.println("Current Interest rate is : " + S[i].showInterestRate());
			
						System.out.println("Enter new interest rate (rate must be <20): ");
						rate = sc.nextFloat();
						
						while(rate>20 || rate<0.1)
						{
							System.out.println("Enter valid rate!");
						}
						
						S[i].setInterestRate(rate);		//set interest rate
						S[i].createFile();
						break;				
					}
				}
				break;
			}
			
			case 5:			//set overdraft limit for current account
			
			if(cj == 0)		//check whether account details entered or not
			{
				System.out.println("No account crated!");
				break;
			}
			
			else
			{
				System.out.println("Enter current Account no.");
				ac_no = sc.nextDouble();
			
			
				for(i=0; i<c; i++)
				{
					if(ac_no == C[i].getAccountNo())
					{
						System.out.println("Your current overdraft liit is : "+C[i].showOverdraftLimit());
			
						System.out.println("Enter new Overdraft limit for this account : ");
						limit = sc.nextLong();
			
						C[i].setOverdraftLimit(limit);	
						C[i].createFile();
						break;			
					}
				}
			}
			break;
			
			case 6:			//credit or withdraw rupees
			
			System.out.println("Press 1: Savings account");
			System.out.println("Press 2: Current account");
			o1 = sc.nextByte();
			
				switch(o1)
				{
					case 1:			//for current account
					if(sj == 0)		//check whether account details entered or not
					{
						System.out.println("No account crated!");
						break;
					}
					else
					{
						System.out.println("Enter savings Account no.");
						ac_no = sc.nextDouble();
			
						for(i=0; i<s; i++)
						{
							if(ac_no == S[i].getAccountNo())	//check account no equal or not
							{
								System.out.println("Press 1: Credit");
								System.out.println("Press 2: withdraw");
								o2 = sc.nextByte();
								 
								if(o2 == 1)
								{
									System.out.println("Enter amount for credit : ");
									credit = sc.nextFloat();
									
									try
									{
										S[i].credit(credit);	//call credit function
										S[i].createFile();
									}
									
									catch(Credit_Withdraw_Exception e)
									{
										System.out.println(e);
									}
									
									break;	
								}
						
								else
								{
									System.out.println("Enter amount for withdraw : ");
									withdraw = sc.nextFloat();
							
									try
									{
										S[i].withdraw(withdraw);//call saving class's withdraw function
										S[i].createFile();
									}
									catch(Credit_Withdraw_Exception e)
									{
										System.out.println(e);
									}
									
									break;
								}
								
							}
							else
							{
								continue;
							}
						}
					}
					break;
					
					
					case 2:				//for current account
					
						if(cj == 0)		//check whether account details entered or not
						{
							System.out.println("No account crated!");
							break;
						}
						else
						{
							System.out.println("Enter current Account no.");
							ac_no = sc.nextDouble();
			
							for(i=0; i<c; i++)
							{
								if(ac_no == C[i].getAccountNo())
								{
									System.out.println("Press 1: Credit");
									System.out.println("Press 2: withdraw");
									o2 = sc.nextByte();
									 
									if(o2 == 1)
									{
										System.out.println("Enter amount for credit : ");
										credit = sc.nextFloat();
										
										try{
											C[i].credit(credit);
											C[i].createFile();
										}
										
										catch(Credit_Withdraw_Exception e)
										{
											System.out.println(e);
										}
										break;	
									}
						
									else
									{
										System.out.println("Enter amount for withdraw : ");
										withdraw = sc.nextFloat();	//call current class's withdraw function
										
										try{
											C[i].withdraw(withdraw);
											C[i].createFile();
										}
										
										catch(Credit_Withdraw_Exception e)
										{
											System.out.println(e);
										}
										
										break;	
									}
								}
					
								else
								{
									continue;
								}
							}
						}
					break;
				}
				break;
				
			case 7:					//view accounts
			
			System.out.println("Press 1: Savings account");
			System.out.println("Press 2: Current account");
			o1 = sc.nextByte();
		
			switch(o1)
			{
				case 1:				//for saving account
					if(sj == 0)		//check whether account details entered or not
					{
						System.out.println("No Saving account found!");
						break;
					}
					else
					{
						System.out.println("\nSaving accounts");	
						for(i=0;i<s;i++)
						{
							
							String fileName = S[i].getStringAccountNo();
							File file = new File(fileName);
							FileInputStream inputs = null;	
							
							if(file.isFile())			//check if file is exist or not
							{
								try
								{
									//byte[] buffer = new byte[length];
									inputs = new FileInputStream(fileName);
									int k;
									for(k=0;k!=-1;)
									{
										k = inputs.read();
										System.out.print((char)k);
									}
								}
						
								catch(IOException e) 
								{
								    System.out.println("Error reading to file '"+ fileName + "'\n");
								}
								
								finally
								{
									try{
										inputs.close();
									}
									catch(Exception e)
									{
										System.out.println("Error : " + e + "\n");
									}
								}
							}
							else
							{
								System.out.println("File Doesn't Exist!\n");
							}
						}
					}
					break;
			
				case 2:				//for current account
					if(cj == 0)		//check whether account details entered or not
					{
						System.out.println("No current account found!");
						break;
					}
					else
					{
						System.out.println("\nCurrent accounts");	
						for(i=0;i<c;i++)
						{
							String fileName = C[i].getStringAccountNo();
							File file = new File(fileName);
							int length = (int)file.length();
							
							FileInputStream inputs;
							
							if(file.isFile())
							{							
								try
								{
									inputs = new FileInputStream(fileName);
									int k;
									for(k=0;k!=-1;)
									{
										k = inputs.read();
										System.out.print((char)k);
									}
								
									inputs.close();
								}
						
								catch(IOException e) 
								{
								    System.out.println("Error reading to file '"+ fileName + "'\n");
								}
							}
							else
							{
								System.out.println("File Doesn't Found!\n");
							}
						}
					}
					break;	
			}
			break;
			
			case 8:				//calculate interest rate for saving account
				if(sj == 0)
				{
					System.out.println("No account crated!");
					break;
				}
				else
				{
					System.out.println("Enter saving Account no.");
					ac_no = sc.nextDouble();

					for(i=0; i<s; i++)
					{
						if(ac_no == S[i].getAccountNo())
						{
							S[i].interest();		//calculate interest for saving account
							break;
						}
		
						else
						{
							continue;
						}
					}
				}
				break;
					
			case 9:				//delete account file
			System.out.println("Press 1: Savings account");
			System.out.println("Press 2: Current account");
			o1 = sc.nextByte();
			
				switch(o1)
				{
					case 1:			//for saving account
					if(sj == 0)		//check whether account details entered or not
					{
						System.out.println("No account crated!");
						break;
					}
					else
					{
						System.out.println("Enter savings Account no.");
						ac_no = sc.nextDouble();
			
						File file = new File(String.valueOf(ac_no));
						
						if(file.isFile())
						
						{
							for(i=0; i<s; i++)
							{
								if(ac_no == S[i].getAccountNo())	//check account no equal or not
								{
									S[i].deleteAccount();								
								}
						
								else
								{
									continue;
								}
							}
						}
						else
						{
							System.out.println("File Doesn't exist!\n");
						}
					}
					break;
					
					
					case 2:			//for current account 
					
						if(cj == 0)		//check whether account details entered or not
						{
							System.out.println("No account crated!");
							break;
						}
						else
						{
							System.out.println("Enter current Account no.");
							ac_no = sc.nextDouble();
							
							File file = new File(String.valueOf(ac_no));
							
							if(file.isFile())
							{							
								for(i=0; i<c; i++)
								{
									if(ac_no == C[i].getAccountNo())
									{
										C[i].deleteAccount();
									}
					
									else
									{
										continue;
									}
								}
							}
							else
							{
								System.out.println("File doesn't exist!\n");
							}
						}
					break;
				}
				break;
				
			default:
				if(option != 0)
				System.out.println("Enter Valid Number!");
				
				break;
		}
		}while(option != 0);
	}
}