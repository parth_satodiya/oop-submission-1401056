#include <iostream>
#include <cstdlib>

using namespace std;


class poly
{
	public:
	int a;
	int *Co;
	int *Ex;
	int min,max;

	poly()					//constrator
	{
		a=0;

		Co = new int[0];
		Ex = new int[0];
	}

	poly(int);							//parameterise constrator

	friend istream &operator >>(istream &input,poly &A)			//operator '>>' overloading
	{
		int i;
		cout<<"__________________________"<<endl;

		for(i=0;i<A.a;i++)
		{
			cout<<endl<<"Enter x^"<< i <<" term's co-efficient : ";
			input>>A.Co[i];
			A.Ex[i]=i;
		}
		return input;
	}

	friend ostream &operator <<(ostream &out,poly &A)			//operator '<<' overloading
	{
		int i;
		cout<<"___________________________"<<endl;

		for(i=0;i<A.a;i++)
		{
			if(i==(A.a)-1)
			{
			out << "("<< A.Co[i]<< "x^"<< A.Ex[i] <<")";
			}

			else
			{
			out << "("<< A.Co[i]<< "x^"<< A.Ex[i] <<") + ";
			}
		}
		return out;
	}

	poly &operator +(poly &B)						//operator '+' overloading
	{
		int i,j;
		poly Temp(max);

		for(i=0 ; i<min ; i++)
		{
			Temp.Co[i] = Co[i] + B.Co[i];
			Temp.Ex[i] = i;
		}

		if(max==a)
		{
			for(i=min ; i<max ; i++)
			{
				Temp.Co[i] = Co[i];
				Temp.Ex[i] = i;
			}
		}

		else
		{
			for(i=min ; i<max ; i++)
			{
				Temp.Co[i] = B.Co[i];
				Temp.Ex[i] = i;
			}
		}
		return Temp;
	}

	poly &operator *(int &r)					//operator '*' overloading
	{
		int i;
		poly Temp(a);

        for(i=0;i<Temp.a;i++)
		{
            Temp.Ex[i] = Ex[i];
			Temp.Co[i] = r * Co[i];
		}
		return Temp;
	}

	poly &operator -()				//unary operator '<<' overloading
	{
		int z,i;
		poly Temp(a);

		for(i=0;i<Temp.a;i++)
		{
            Temp.Ex[i] = Ex[i];
			Temp.Co[i] = - Temp.Co[i];
		}
		return Temp;
	}
};


poly::poly(int n)
{
	a=n;

	Co = new int[n];
	Ex = new int[n];
}



int main()
{
	int a,i,j,n,o,x,y,z,v;

	cout<<"How many polynomial you want to enter : ";
	cin>>n;

	poly P[n];

	do{

	cout<<"Press 1: Add Exponantial and Co-efficient"<<endl;
	cout<<"Press 2: Print Polynomial"<<endl;
	cout<<"Press 3: Add two Polynomial"<<endl;
	cout<<"Press 4: Multiply Polynomial with scalar value"<<endl;
	cout<<"Press 5: Take negative of the Polynomial"<<endl;
	cout<<"Press 0: Exit"<<endl;
	cin>>o;

		switch(o)
		{
		case 1:
		{
			for(i=0;i<n;i++)
			{
				cout<<"how many term " << (i+1) << " polynomial enter :";
				cin>>a;
				P[i]=poly(a);
			}

			for(i=0;i<n;i++)
			{
				cin>>P[i];
			}
			break;
		}

		case 2:
		{
			for(i=0;i<n;i++)
			{
				cout<<P[i];
				cout<<"\n";
			}
			break;
		}

		case 3:

		{
			cout<<"Enter First Polynomial : ";
			cin>>y;

			cout<<"Enter Second Polynomial : ";
			cin>>z;

			if(P[y-1].a>P[z-1].a)
			{
				P[y-1].max=P[y-1].a;
				P[y-1].min=P[z-1].a;
			}

			else
			{
				P[y-1].max=P[z-1].a;
				P[y-1].min=P[y-1].a;
			}

			v=P[y-1].max;
			poly Add(v);

			Add = P[y-1] + P[z-1];
			cout<<Add;
			cout<<"\n";

			break;
		}
		case 4:
		{
			cout<<"Enter polunomial no. for which you multiply by scalar value : ";
			cin>>y;

			if(y>n)
			{
				cout<<"No polynomial entered.!"<<endl;
			}

			else
			{
				cout<< "Enter value of scalar : ";
				cin>>z;
				cout<<endl;

				v = P[y-1].a;
				poly Scalar(v);

				Scalar = P[y-1] * z;
				cout<<Scalar;
				cout<<"\n";
			}

			break;
		}
		case 5:
		{
			cout<<"Enter polunomial no. for which you want to become negative : ";
			cin>>y;

			if(y>n)
			{
				cout<<"No polynomial entered.!"<<endl;
			}

			else
			{
				Negative = -P[y-1];
				cout<<Negative;
				cout<<"\n";
			}

			break;

		}
		}
	}while(o!=0);
}

