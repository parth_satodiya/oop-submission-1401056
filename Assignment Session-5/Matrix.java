import java.util.Scanner;

class mat
{
	Scanner S=new Scanner(System.in);

	int row,coloum,i,j,k;
	int[][] A;					//create Array in class

	void createMatrix(int r, int c)
	{
		row=r;
		coloum=c;
		A=new int[r][c];
	}

	void addDetail()					/to add details
	{
		for(i=0;i<row;i++)
		{
			for(j=0;j<coloum;j++)
			{
				System.out.print("Enter ( " + (i+1) + "," + (j+1) + ") element of the metrix");
				A[i][j] = S.nextInt();
				System.out.print("\n");
			}
		}
	}

	void viewElement(int i,int j)			/to view matrix
	{
		if(i>row || i<=0 || j>coloum || j<=0)
		{
			System.out.println("Please Enter Appropriate rows or coloum number.!");
			System.out.print("\n");
		}

		else
		{	
			System.out.print("\n");
			System.out.println("( " + i + "," + j + ") element of the metrix is : " + A[i-1][j-1]);
		}
	}

	void viewMatrix()
	{
		for(i=0;i<row;i++)
		{
			for(j=0;j<coloum;j++)
			{
				System.out.print(A[i][j] + " ");
			}	
			System.out.println("\n----------");
		}
	}

	void viewTrMatrix()			//to print transpose of the matrix
	{
		for(i=0;i<coloum;i++)
		{
			for(j=0;j<row;j++)
			{
				System.out.print(A[j][i] + " ");
			}	
			System.out.println("\n----------");
		}
	}

	void addMatrix(mat M, mat Add)			// addition of two matrix
	{
		for(i=0;i<row;i++)
		{
			for(j=0;j<coloum;j++)
			{
				Add.A[i][j] = this.A[i][j] + M.A[i][j];
			}
		}
		
		for(i=0;i<row;i++)
		{
			for(j=0;j<coloum;j++)
			{
				System.out.print(Add.A[i][j] + " ");
			}
			System.out.println("\n----------");
		}
	}

	void multiplyMatrix(mat M, mat Multi)		// multyply two matrix
	{
		for(i=0 ; i<Multi.row ; i++)
		{
			for(j=0 ; j<Multi.coloum ; j++)
			{	
				Multi.A[i][j] = 0;				
				for(k=0 ; k<M.row ; k++)
				{ 
					Multi.A[i][j] = Multi.A[i][j] + (this.A[i][k] * M.A[k][j]);
				}
			}
		}

		for(i=0 ; i<Multi.row ; i++)
		{
			for(j=0 ; j<Multi.coloum ; j++)
			{
				System.out.print(Multi.A[i][j] + " ");
			}
			System.out.println("\n----------");
		}
	}

	void scalarMatrix(int k)		// multiply matrix by scalar
	{
		for(i=0 ; i<this.row ; i++)
		{
			for(j=0 ; j<this.coloum ; j++)
			{
				System.out.print( (k*A[i][j]) + " ");
			}
			System.out.println("\n----------");
		}
	}
}

class Matrix
{
	public static void main(String argv[])
	{	
	
		Scanner S=new Scanner(System.in);	

		int o,n,i,j,check=0;
		int r,c,fr,fc;

		System.out.println("Howmany matrix Do you want to enter : ");
		n=S.nextInt();

		mat[] M=new mat[n];			//create array of object

		do{

		System.out.println("_________________________________________________________");
		System.out.print("\n");

		System.out.println("Press 0: Exit 			   : ");
		System.out.println("Press 1: Create matrix and add Element : ");
		System.out.println("Press 2: View Element of the matrix    : ");
		System.out.println("Press 3: View matrix 		   : ");
		System.out.println("Press 4: Create transpose of the matrix : ");
		System.out.println("Press 5: Add two matrix : ");
		System.out.println("Press 6: Multiply two matrix : ");
		System.out.println("Press 7: Multiply matrix by scalar value : ");
		o=S.nextInt();
		System.out.print("\n");
		
			switch(o)
			{
			case 1:

				for(i=0;i<n;i++)
				{
					System.out.print("Enter no of rows for matrix : ");
					r=S.nextInt();
					System.out.print("\n");

					System.out.print("Enter no of coloums for matrix : ");
					c=S.nextInt();
					System.out.print("\n");
				
					M[i]=new mat();
					M[i].createMatrix(r,c);
					M[i].addDetail();
	
					check++;
				}
				break;

			case 2:
	
				System.out.print("Enter no of matrix in which you want to find element : ");
				i=S.nextInt();	
				System.out.print("\n");

				if(i<=0 || i>n)
				{
					System.out.println("Enter Valid Number.!");
					System.out.print("\n");
					break;
				}
	
				else
	
				{
				System.out.println("Enter row to find element    : ");
				fr=S.nextInt();

				System.out.println("Enter coloum to find element : ");
				fc=S.nextInt();

				M[i-1].viewElement(fr,fc);
				}

				break;

			case 3:
				

				System.out.print("Enter no of matrix in which you want to watch : ");
				i=S.nextInt();	
				System.out.print("\n");

				if(i<=0 || i>n)
				{
					System.out.println("Enter Valid Number.!");
					System.out.print("\n");
					break;
				}

				else if(check==0)
				{	
					System.out.println("No Matrix Entered.!");
					System.out.print("\n");
					break;
				}
				
				else
	
				{
					M[i-1].viewMatrix();
					System.out.println("\n");
				}

				break;

			case 4:
					
				System.out.print("Enter no of matrix in which you want to see in transpose form : ");
				i=S.nextInt();	
				System.out.print("\n");

				if(i<=0 || i>n)
				{
					System.out.println("Enter Valid Number.!");
					System.out.print("\n");
					break;
				}
				
				else if(check==0)
				{	
					System.out.println("No Matrix Entered.!");
					System.out.print("\n");
					break;
				}
	
				else
	
				{
					
					M[i-1].viewTrMatrix();
				  	System.out.println("\n");

				}
				break;

			case 5:	
				System.out.print("Enter first matrix for add : ");
				i=S.nextInt();
				
				System.out.print("Enter second matrix for add : ");
				j=S.nextInt();	
				System.out.print("\n");		

				if(i<=0 || i>n || j<=0 || j>n)
				{
					System.out.println("Enter Valid Number.!");
					System.out.print("\n");
					break;
				}
				
				else if(check<=1)
				{	
					System.out.println("No sufficient Matrix Entered.!");
					System.out.print("\n");
					break;
				}

				else if(M[i-1].row!=M[i-1].coloum || M[i-1].row!=M[i-1].coloum ||M[i-1].row!=M[j-1].row || M[i-1].coloum!=M[j-1].coloum)
				{
					System.out.println("Addition operation envalid.!");
					System.out.print("\n");
					break;

				}
		
				else
				{
					int x;
					x=M[i-1].row;

					mat Add=new mat();
					Add.createMatrix(x,x);
					
					M[i-1].addMatrix(M[j-1],Add);
				}
				break;
			
		
				case 6:
					System.out.print("Enter first matrix for multiply : ");
					i=S.nextInt();
				
					System.out.print("Enter second matrix for multiply : ");
					j=S.nextInt();	
					System.out.print("\n");
					
					if(i<=0 || i>n || j<=0 || j>n)
					{
						System.out.println("Enter Valid Number.!");
						System.out.print("\n");
						break;
					}
					
					else if(check<=1)
					{	
						System.out.println("No sufficient Matrix Entered.!");
						System.out.print("\n");
						break;
					}

					else if(M[i-1].coloum != M[j-1].row)
					{
						System.out.println("Addition operation envalid.!");
						System.out.print("\n");
						break;

					}

					else
					{
						int x,y;
						x=M[i-1].row;
						y=M[j-1].coloum;

						mat Multi=new mat();
						Multi.createMatrix(x,y);
					
						M[i-1].multiplyMatrix(M[j-1],Multi);
					}
				break;
				

				case 7:
					System.out.print("Enter no of matrix in which you want to multiply by scalar : ");
					i=S.nextInt();	
					System.out.print("\n");

					if(i<=0 || i>n)
					{
						System.out.println("Enter Valid Number.!");
						System.out.print("\n");
						break;
					}
				
					else if(check==0)
					{	
						System.out.println("No Matrix Entered.!");
						System.out.print("\n");
						break;
					}
	
					else
	
					{
						System.out.print("Enter scalar value : ");
						j=S.nextInt();


						M[i-1].scalarMatrix(j);
					  	System.out.println("\n");

					}
					break;
				}
		}while(o!=0);
	}
}	
