# README #

This Directory contain Object Oriented c++ and java programs. 

### What is this repository for? ###

* This programs will help you to understand basics of **Object Oriented** programming using **c++** and **java**.

### How do I get set up? ###

* I created some folder. In this you can found programs.
* Outside the folders I upload some file like How to install java compiler in *Ubuntu*, What is         programming paradigms etc.
* You can **run** these programs by copy or download in your PC or Laptop.

### Contribution guidelines ###

* If you want to improve these codes then you can do it.
* Also you can write some comments in any programs.
* I hope these programs will help you to understand Object Oriented Programming.

### Who do I talk to? ###

* You can **contact** me at parth.satodiya@gmail.com