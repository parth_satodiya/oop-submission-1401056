#include<iostream>

using namespace std;

class Queue
{
	private:
	int size;
	int start,curr;
	int *queue;
	int i;
	
	public:
	Queue()							//constructor
	{
		size=0;
		start=0;
		curr=0;
	}
	
	Queue(int a)						//perameterise constructor
	{
		size=a;
		queue=new int[size];
		start=0;
		curr=0;
	}
	
	void operator +(int &b)					//binary operator +
	{
		if(curr!=size)
		{
			queue[curr]=b;
			curr++;
		}
		
		else
		{
			cout<<"Maximum limit reached"<<endl;
		}
	}
	
	void operator--()					//unary operator --
	{
		if(curr==start)
		{
			cout<<"There is no more element to delete.!"<<endl;
		}
		else
		{
			cout<<"element "<<queue[start]<<" is removed.!"<<endl;
			
			queue[start] = 0;
			curr = curr - 1;
			
			for(i=0;i<size;i++)			//shift all element to previous place
			{
				queue[i]=queue[i+1];
			}	
			queue[size-1]=0;
		}
	}
	
	friend ostream &operator <<(ostream &out, Queue A);	//friend operator for show queue
		
};

ostream &operator <<(ostream &out, Queue A)     		//defination of << operator
	{
		int j;

		for(j=0;j<A.curr;j++)
		{
			out<<A.queue[j]<<" ";
		}
		out<<endl;
		
		return out;
	}

int main()							//main function
{
	int n,add,choice;
	
	cout<<"How many number of queue you want to build ? "<<endl;
	cin>>n;
	Queue Q=Queue(n);

	do{	
		cout<<endl<<"Press 0 : Exit : ";
		cout<<endl<<"Press 1 : Add number in queue : ";
		cout<<endl<<"Press 2 : Dequeue element : ";
		cout<<endl<<"Press 3 : Show Queue : ";
	
		cin>>choice;
		cout<<endl;
		switch(choice)
		{
			case 1:							//queue elements
				cout<<"Enter element ( except 0 ) : ";
				cin>>add;
				
				Q + add;
				break;
				
			case 2:							//unqueue elements
				--Q;	
		
				break;			
			
			case 3:							//print all elements
				cout<<Q;
				break;
				
		}
	}while(choice!=0);
	
	return 0;
}
		

