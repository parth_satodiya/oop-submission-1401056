import java.util.Scanner;

class Media_record							//main class
{
	
	public static void main(String argv[])
	{
	
	Scanner sc=new Scanner(System.in);
	
	
	int choice,i,nb,nc;
	
	System.out.print("\n");
	System.out.print("Howmany book record you want to enter : ");		//take no. of book
	nb=sc.nextInt();
	
	System.out.print("Howmany CD record you want to enter : ");		//take no. of CD
	nc=sc.nextInt();
	
	book[] B;	
	cd[] C;	
	B=new book[nb];
	C=new cd[nc];
	
	do{
	
	System.out.print("---------------------------------------------");
	System.out.print("\n");
	System.out.println("Press 0 : Exit ");
	System.out.println("Press 1 : Enter record of book ");
	System.out.println("Press 2 : Enter record of CD ");
	System.out.println("Press 3 : Print record of book ");
	System.out.println("Press 4 : Print record of CD ");
	choice = sc.nextInt();
	System.out.println("_____________________________________________");
	System.out.print("\n");
	
	switch(choice)
	{		
		case 1:								//Enter data of books
		for(i=0;i<nb;i++)
		{	
			String t,a;
			int y,page;
			float price;
			
			System.out.print("Enetr Book title : " );
			t=sc.next();
			System.out.print("\n");
			
			System.out.print("Enter year of publication of book : " );
			y=sc.nextInt();
			System.out.print("\n");
			
			System.out.print("Enter name author : " );
			a=sc.next();
			System.out.print("\n");
				
			System.out.print("Enter Number of pages in book : " );
			page=sc.nextInt();
			System.out.print("\n");
			
			System.out.print("Enter Book price : " );
			price=sc.nextFloat();
			System.out.println("_____________________________________________");
			System.out.print("\n");
			
			B[i]=new book();
			B[i].set_data(t,y,price);
			B[i].set_book_data(a,page);
		}
		break;
		
		case 2:								//enter data for cd
		for(i=0;i<nc;i++)
		{	
			String t;
			int y,size;
			float time,price;
			
			System.out.print("Enetr CD title : " );
			t=sc.next();
			System.out.print("\n");
			
			System.out.print("Enter year of publication of CD : " );
			y=sc.nextInt();
			System.out.print("\n");
			
			System.out.print("Enter play time of CD : " );
			time=sc.nextFloat();
			System.out.print("\n");
			
			System.out.print("Enter size of CD in MB : " );
			size=sc.nextInt();
			System.out.print("\n");
			
			System.out.print("Enter CD price : " );
			price=sc.nextFloat();
			
			System.out.println("_____________________________________________");
			System.out.print("\n");
			
			C[i]=new cd();
			C[i].set_data(t,y,price);
			C[i].set_CD_data(size,time);
		}
		break;
		
		case 3:
		if(B[0]!=null)
		{
			for(i=0;i<nb;i++)
			{
				B[i].get_book_data();				//print all book data
			}
		}
		
		else
		{
			System.out.println("Please Enter Book Record First.!");
			System.out.print("\n");
		}
		break;
		
		case 4:								//print all cd data
		
		if(C[0]!=null)
		{
			for(i=0;i<nc;i++)
			{
				C[i].get_CD_data();
			}
		}
		
		else
		{
			System.out.println("Please Enter CD Record First.!");
			System.out.print("\n");
		}    
		break;
	}
		
	}while(choice != 0);
	}
}


class Media
{
	private String title;
	private int year_of_pub;
	private float price;

	Media()
	{
		title=null;
		year_of_pub=0;
		price=0;
	}

	public void set_data(String t,int y,float p)				//parent class
	{
		title=t;
		year_of_pub=y;
		price=p;
	}

	public void get_title()
	{
		System.out.print(" " + title);
	}	
		
	public void get_year_of_publication()
	{
		System.out.print(" " + year_of_pub);
	}
	
	public void get_price()
	{
		System.out.print(" Rs." + price);
	}
}

class book extends Media							//child class
{
	private String author;
	private int no_of_pages;

	book()
	{
		author = null;
		no_of_pages = 0;
	}
	
	public void set_book_data(String a,int p)
	{
		author=a;
		no_of_pages=p;
	}
	
	void get_book_data()
	{
		System.out.print("Book title : " );
		get_title();
		System.out.print("\n");
		
		System.out.print("year of publication of book : ");
		get_year_of_publication();
		System.out.print("\n");
		
		System.out.println("Book author : " +author);
		
		System.out.println("Number of pages in book : " + no_of_pages);
		
		System.out.print("Book price : ");
		get_price();
		System.out.print("\n");
		System.out.println("_____________________________________________");
		System.out.print("\n");
	}
} 

class cd extends Media								//child class
{
	int size_in_MB;
	float play_time;
	
	cd()
	{
		size_in_MB=0;
		play_time=0;
	}
	
	void set_CD_data(int s,float p)
	{
		size_in_MB = s;
		play_time = p;
	}
	
	void get_CD_data()
	{
		System.out.print("CD title : ");
		get_title();
		System.out.print("\n");
		
		System.out.print("year of publication of CD : " );
		get_year_of_publication();
		System.out.print("\n");
		
		System.out.println("Play time of CD : " + play_time +" min");
		
		System.out.println("Size of CD in MB : " + size_in_MB);
		
		System.out.print("Book price : ");
		get_price();
		
		System.out.print("\n");
		System.out.println("_____________________________________________");
		System.out.print("\n");
	}
}

