#include<iostream>
#include<cstdlib>

using namespace std;

int ac=123;                                                     //ac init as global variable to provide auto acount no.

class Banking                                                     //create class'Bank'
{
private:
    struct node                                                 //create  struct node with for linked list
    {
    char branch_city[20];
	char branch_name[20];

	char client_name[25];
	char client_address[50];
	char client_phone[12];

    int account_no;
	int balance;

    node *next;
	}*start,*curr;


public:
    Banking()
    {
        curr=NULL;                                              //constructor
        start=NULL;                                             //inaltial curr and start
    }

    void getDetails();
    void printAllDetails();
    void searching();
    void deleteAc();                                              //functions
    void updateDetails();
    void debitCreditBalance();
};

//____________________________________________________________________________________________________________________


void Banking::getDetails()
{
    int a;

    do{
    node *temp;                                                        //creat new temporary node
    temp=new (struct node);

    cout<<endl<<endl<<"Enter client Name : ";
	cin>>temp->client_name;

	cout<<endl<<"Enter client address : ";
	cin>>temp->client_address;

	cout<<endl<<"Enter client phone number : ";
	cin>>temp->client_phone;

	cout<<endl<<"Enter account branch city : ";
	cin>>temp->branch_city;

	cout<<endl<<"Enter branch name : ";
	cin>>temp->branch_name;

	cout<<endl<<"Howmuch rupees you want to keep in this account : ";
	cin>>temp->balance;

	temp->next=NULL;                                //declare next node as null

	if(start==NULL)
    {
        start=curr=temp;
    }
    else
    {
        curr->next=temp;                                                //shift 'curr' node to temp node
        curr=temp;
    }

    temp->account_no=ac;

    cout<<endl<<"Your Account No. is : "<<temp->account_no<<endl;
    ac++;                                                               //allocate account no.

    cout<<endl<<"Do you want to add another account? : "<<endl;
    cin>>a;
}while(a==1);
}

void Banking::printAllDetails()                                             //fund=ction for print all accounts
{
    node *ptr;
    for(ptr=start;ptr!=NULL;ptr=ptr->next)
    {
        cout<<endl<<"Client Name : "<<ptr->client_name;
        cout<<endl<<"Client Address : "<<ptr->client_address;
        cout<<endl<<"client Phone no. : "<<ptr->client_phone;
        cout<<endl<<"Branch City : "<<ptr->branch_city;
        cout<<endl<<"Branch Name : "<<ptr->branch_name;
        cout<<endl<<"client Account Balance : "<<ptr->balance<<endl<<endl;
    }
}

void Banking::searching()
{
            int acc;
            node *temp;                                                 //temparary node for searching account no.
            temp=start;                                                 //Searching start from head(first node).

            cout<<endl<<endl<<"Enter Account no. you want to search : "<<endl;
            cin>>acc;

            do{

                if(temp->account_no==acc)
                {
                    cout<<endl<<"client Name : "<<temp->client_name;
                    cout<<endl<<"client Address : "<<temp->client_address;
                    cout<<endl<<"client Phone no. : "<<temp->client_phone;
                    cout<<endl<<"Branch City : "<<temp->branch_city;
                    cout<<endl<<"Branch Name : "<<temp->branch_name;
                    cout<<endl<<"client Account Balance : "<<temp->balance<<endl;

                    break;
                }
                temp=temp->next;                                        //incrementation of node


            }while(temp!=NULL);
}

void Banking::deleteAc()                                          //delete account
{
            int acc;
            node *temp,*t;
            temp=start;

            cout<<endl<<endl<<"Enter Account no. you want to delete : "<<endl;
            cin>>acc;

            if(temp->account_no==acc)                      //If we want to delete first node then
            {                                             // store data of 'start' node in temparry node and then delete data of start
                t=start;                                   //and temparary var store address os start'snext node.
                start=NULL;
                temp=t->next;
                return;
            }

            while(temp->next!=NULL && (temp->next)->account_no != acc) //searching account till one of the condition is become false
        {
           temp=temp->next;                                         //increment.
        }
           if(temp->next==NULL)
           {
               cout<<endl<<"Doesn't exist";
               return;
           }

        t=temp->next;
        temp->next=NULL;
        temp=t->next;

        free(t);
        return;
}

void Banking::updateDetails()                   //Update Details
{
    int acc;
    node *temp;
    temp=start;

    cout<<endl<<endl<<"Enter Account no. you want to search : "<<endl;
    cin>>acc;

    do{

        if(temp->account_no==acc)                       //Search account no.
        {
            cout<<"You can update only Address, Phone no., branch name and branch city."<<endl;

            cout<<endl<<"Enter client address : ";
            cin>>temp->client_address;

            cout<<endl<<"Enter client phone number : ";
            cin>>temp->client_phone;

            cout<<endl<<"Enter account branch city : ";
            cin>>temp->branch_city;

            cout<<endl<<"Enter branch name : ";
            cin>>temp->branch_name;

            break;
        }
        temp=temp->next;                                //node increment


    }while(temp!=NULL);
}

void Banking::debitCreditBalance()                  //enter credit/debit details in account
{
    int acc,d,b;
    node *temp;
    temp=start;

    cout<<endl<<endl<<"Enter Account no. you want to search : "<<endl;
    cin>>acc;

    cout<<"What do you want to do?";
    cout<<"Press 1-->Credit"<<endl;
    cout<<"Press 2-->Debit"<<endl;
    cin>>d;

    do{

        if(temp->account_no==acc)
        {
            if(d==1)
            {
                cout<<"How much money you want to credit:";
                cin>>b;

                cout<<"Successfully credited...Your new balance is "<<temp->balance+b;

                temp->balance=temp->balance+b;
            }

            else if(d==0)
            {
               cout<<"How much money you want to Withdraw:";
                cin>>b;

                cout<<"Successfully withdraw...Your new balance is "<<temp->balance-b;

                temp->balance=temp->balance-b;
            }

            break;
        }
        temp=temp->next;


    }while(temp!=NULL);


}

//______________________________________________________________________________________________________________________


int main()
{
    int o;
    Banking A;

do
{

    cout<<endl<<endl<<"_________________________________________________________"<<endl;
    cout<<endl<<"Press 1: Enter new account : ";
    cout<<endl<<"Press 2: Print existing account : ";
    cout<<endl<<"Press 3: Find Record : ";
    cout<<endl<<"Press 4: Delete Record : ";
    cout<<endl<<"Press 5: Update Details : ";
    cout<<endl<<"Press 6: Credit or Withdrawal Of Money :";
    cout<<endl<<"press 7: Exit : "<<endl;
    cin>>o;
    cout<<endl<<endl<<"_________________________________________________________"<<endl;

    switch(o)
    {
    case 1:

        {
            A.getDetails();
            break;
        }
    continue;

    case 2:
        {
            A.printAllDetails();
            break;
        }
    continue;

    case 3:
        {
            A.searching();
            break;
        }
    continue;

    case 4:
        {
            A.deleteAc();
            break;
        }
    continue;

    case 5:
        {
            A.updateDetails();
            break;
        }
    case 6:
        {
            A.debitCreditBalance();
            break;
        }
           }

}while(o<7 && o>0);

    return 0;
}


