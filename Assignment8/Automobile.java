					//Assignment 8
					//program to create, save and update data of vehicles

import java.util.*;
import java.io.*;			//import file library

class Engine implements Serializable		//implement Serializable interface	//class for engine of vehicle
{
	float average, hp;
	String ecompany;
	
	String temp;
	byte[] t;
	
	public void writeEngine()	//write details of vehicle's engine
	{
			Scanner sc = new Scanner(System.in);
			System.out.print("Enter your vehicle's engine's average : ");
			average = sc.nextFloat();
				
	
			System.out.print("Enter your vehicle's engine's company : ");
			ecompany = sc.next();
			
		
			System.out.print("Enter vehicle's engine's horse-power : ");
			hp = sc.nextFloat();
	}
	
	public void updateEngine()	//Update details of your engine
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter your vehicle's engine's new average : ");
		average = sc.nextFloat();
		
		System.out.print("Enter your vehicle's engine's new company : ");
		ecompany = sc.next();
		
		System.out.print("Enter vehicle's engine's new horse-power : ");
		hp = sc.nextFloat();
		
	}
	
	public void showEngine()		//show engine details
	{
		System.out.println("Vehicle's engine's average : " + average);
		
		
		System.out.println("Vehicle's engine's company : " + ecompany);
	
		
		System.out.println("Vehicle's engine's horse-power : " + hp);
	}
}

abstract class Vehicle implements Serializable		//implement Serializable interface	//create abstract class
{
	
	String vcompany, model_nameorno, colour;
	
	String temp, filename;
	byte[] t;
	
	Engine e = new Engine();			//create object of engine class to impliment containership
	
	public void writeVehicle()			//write details of vehicle
	{
	               Scanner sc = new Scanner(System.in);
			
			
			System.out.print("Enter your vehicle's company name : ");
			vcompany = sc.next();
				
	
			System.out.print("Enter your vehicle's model name or number : ");
			model_nameorno = sc.next();
			
		
			System.out.print("Enter vehicle's colour : ");
			colour = sc.next();
			
			
			e.writeEngine();		//call function of engine class
	}	
	
	public void updateVehicle()			//for update details
	{		
			Scanner sc = new Scanner(System.in);
			System.out.print("Enter your new vehicle's company name : ");
			vcompany = sc.next();
				

			System.out.print("Enter your new vehicle's model name or number : ");
			model_nameorno = sc.next();
			
	
			System.out.print("Enter new vehicle's colour : ");
			colour = sc.next();
			
			e.updateEngine();		//call function of engine class
	}
	
	public void showVehicle()		//show vehicle details
	{
		System.out.println("Vehicle's company name : " + vcompany);
		
		System.out.println("Vehicle's model name or number : " + model_nameorno);
		
		System.out.println("Vehicle's colour : " + colour);
		
		System.out.println("Vehicle's engine's average : " + e.average);
		System.out.println("Vehicle's engine's company : " + e.ecompany);
		System.out.println("Vehicle's engine's horse-power : " + e.hp);
	}
}

class TwoWheeler extends Vehicle implements Serializable	//implement Serializable interface	//child class of vehicle class
{
	int cc;
	String temp, filename;
	byte[] t;
	
	public void write2w()		//write function
	{		
		Scanner sc = new Scanner(System.in);			
		System.out.print("Howmany cc(centemeter cube) in your bike : ");
		cc = sc.nextInt();
	}
	
	public void update2w()		//update function
	{
		Scanner sc = new Scanner(System.in);		
		System.out.print("Howmany cc(centemeter cube) in your bike : ");
		cc = sc.nextInt();
	}
	
	public void show2w()		//show function
	{
		showVehicle();
		System.out.println("CC(centemeter cube) in your bike : " + cc + "\n");
	}
}

abstract class FourWheeler extends Vehicle implements Serializable	//implement Serializable interface	//child class of vehicle class
{
	int airbag;
	String audio_video;
	
	String temp, filename;
	byte[] t;
	
		
	
	void write4w()		//write function
	{		
		Scanner sc = new Scanner(System.in);	
		System.out.print("Howmany airbags in your vehicle : ");
		airbag = sc.nextInt();
		
		System.out.print("Enter details of vehicle's audio/video system : ");
		audio_video = sc.next();
	}	
	
	public void update4w()		//update function
	{			
		Scanner sc = new Scanner(System.in);	
		System.out.print("Howmany airbags in your new vehicle : ");
		airbag = sc.nextInt();
		 
		System.out.print("Enter details of new vehicle's audio/video system : ");
		audio_video = sc.next();
	}
	
	public void show4w()		//show function
	{
		System.out.println("Airbags in your vehicle : " + airbag);
		System.out.println("Vehicle's audio/video system : " + audio_video + "\n");
	}
}

class PrivateVehicle extends FourWheeler implements Serializable	//implement Serializable interface	//child class of four wheeler class
{
	int use, seat;
	String temp, filename;
	byte[] t;
	
	
	
	public void write4wPrivate()	//write function
	{			
			Scanner sc = new Scanner(System.in);		
			System.out.print("How many kms of daily use of your private vehicle : ");
			use = sc.nextInt();
		
			System.out.print("How many max member can sit in your private vehicle : ");
			seat = sc.nextInt();
			
	}	
	
	public void update4wPrivate()	//update function
	{
			Scanner sc = new Scanner(System.in);		
			System.out.print("How many kms of daily use of your new private vehicle : ");
			use = sc.nextInt();
	
			System.out.print("How many max member can sit in your new private vehicle : ");
			seat = sc.nextInt();
	}
	
	public void show4wPrivate()	//show function
	{			
			showVehicle();
			show4w();

			System.out.println("kms of daily use of your private vehicle : " + use);			
			System.out.println("Max member can sit in your private vehicle : " + seat);
	}	
}

class CommercialVehicle extends FourWheeler implements Serializable	//implement Serializable interface	//child class of four wheeler class
{
	long licence;
	String temp, filename;
	byte[] t;
	
	void write4wCommercial()	//write function
	{			
		Scanner sc = new Scanner(System.in);		
		System.out.print("Enter your commercial licence no. : ");
		licence = sc.nextLong();
	}	
	
	public void update4wCommercial()	//update function
	{			
		Scanner sc = new Scanner(System.in);		
		System.out.print("Enter your commercial licence no. : ");
		licence = sc.nextLong();
	
	}
	
	void show4wCommercial()		//show function
	{		
		Scanner sc = new Scanner(System.in);		
		showVehicle();				//call show funnction on vehicle
		show4w();				//call show function of four wheeler

		System.out.println("Airbags in your vehicle : " + airbag);
		System.out.println("Vehicle's audio/video system : " + audio_video);	
		System.out.println("Commercial licence no. : " + licence + "\n");
	}		
}

class Automobile		//main class
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		
		byte choice1 = 1, choice2 = 1, choice3 = 1, option;
		String filename;
		File f;
		
		Vehicle v;					//reference of vehicle class
		TwoWheeler tw = new TwoWheeler();		//two wheeler object
		PrivateVehicle pv = new PrivateVehicle();	//private four wheeler object
		CommercialVehicle cv = new CommercialVehicle();		//commicial four wheeler function	
		FourWheeler fw = pv;				//fouorwheeler function
		
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		
		do{
			System.out.println("Press 1 : Enter new record");
			System.out.println("Press 2 : Update record");
			System.out.println("Press 3 : Delete record");
			System.out.println("Press 4 : show record");
			System.out.println("Press any other number : Exit");
			option = sc.nextByte();
		
			switch(option)
			{
				case 1:
				System.out.println("Press 1 --> Two wheeler : ");
				System.out.println("Press any other number --> Four wheeler : ");
				choice1 = sc.nextByte();
		
				if(choice1 == 1)
				{
					v = tw;		// give reference of vehicle to instance of two wheeler class
				}
				else		
				{
					System.out.println("Press 1 --> Private vehicle : ");
					System.out.println("Press any other number --> Public vehicle : ");
					choice2 = sc.nextByte();
			
					if(choice2 == 1)
					{
						v = pv;		// give reference of vehicle to instance of private four wheeler class
						fw = pv;	// give reference of four wheeler to instance of private four wheeler class
					}
			
					else
					{
						v = cv;		// give reference of vehicle to instance of commercial four wheeler class
						fw = cv;	// give reference of four wheeler to instance of commercial four wheeler class
					}
				}
		
				System.out.print("Enter file name : ");		//get file name
				filename = sc.next();
				
				v.writeVehicle();
			
				try{	
					fos = new FileOutputStream(filename, false);		
					oos = new ObjectOutputStream(fos);				
			
					oos.writeObject(v);			//write whole vehicle object to file
					oos.close();
				}
		
				catch(Exception e)
				{
					System.out.println("Error : " + e + "\n");
				}
				
				if(choice1 == 1)
				{
					tw.write2w();
					
					try{	
					fos = new FileOutputStream(filename, false);		
					oos = new ObjectOutputStream(fos);				
			
					oos.writeObject(tw);			//write whole twowheeler object to file
					oos.close();
					}
		
					catch(Exception e)
					{
						System.out.println("Error : " + e);
					}
				}
				
				else
				{
					fw.write4w();
			
					try{	
					fos = new FileOutputStream(filename, false);		
					oos = new ObjectOutputStream(fos);				
			
					oos.writeObject(fw);		//write whole four wheeler object to file
					oos.close();
					}
		
					catch(Exception e)
					{
						System.out.println("Error : " + e);
					}
					
					if(choice2 == 1)
					{
						pv.write4wPrivate();		//write whole private four wheeler object to file
						try{	
							fos = new FileOutputStream(filename, false);		
							oos = new ObjectOutputStream(fos);				
			
							oos.writeObject(pv);
							oos.close();
						}
		
						catch(Exception e)
						{
							System.out.println("Error : " + e);
						}
					}
					else
					{
						cv.write4wCommercial();		//write whole commercial four wheeler object to file
						try{	
							fos = new FileOutputStream(filename, false);		
							oos = new ObjectOutputStream(fos);				
			
							oos.writeObject(cv);
							oos.close();
						}
		
						catch(Exception e)
						{
							System.out.println("Error : " + e);
						}
					}
				}
				break;
			
				case 2:
					System.out.print("Enter filename you want to update : ");
					filename = sc.next();
					
					f = new File(filename);
					
					if(f.exists() && !f.isDirectory())
					{
						System.out.println("Press 1 --> Two wheeler : ");
						System.out.println("Press any other number --> Four wheeler : ");
						choice1 = sc.nextByte();
		
						if(choice1 == 1)
						{
							v = tw;		// give reference of vehicle to instance of two wheeler class
						}
						else		
						{
							System.out.println("Press 1 --> Private vehicle : ");
							System.out.println("Press any other number --> Public vehicle : ");
							choice2 = sc.nextByte();
			
							if(choice2 == 1)
							{
								v = pv;		// give reference of vehicle to instance of private four wheeler class
								fw = pv;	// give reference of four wheeler to instance of private four wheeler class
							}
			
							else
							{
								v = cv;		// give reference of vehicle to instance of commercial four wheeler class
								fw = cv;	// give reference of four wheeler to instance of commercial four wheeler class
							}
						}
		
						v.updateVehicle();		//call update function of vehicle
						try{	
							fos = new FileOutputStream(filename, false);		
							oos = new ObjectOutputStream(fos);				
			
							oos.writeObject(v);	//write vehicle object to file
							oos.close();
						}
		
						catch(Exception e)
						{
							System.out.println("Error : " + e);
						}
						
						
						if(choice1 == 1)
						{
							tw.update2w();
							try{	
								fos = new FileOutputStream(filename, false);		
								oos = new ObjectOutputStream(fos);				
		
								oos.writeObject(tw);	//write two wheeler object to file
								oos.close();
							}
							catch(Exception e)
							{
								System.out.println("Error : " + e);
							}
						}
						else
						{
							fw.update4w();
							try{	
								fos = new FileOutputStream(filename, false);		
								oos = new ObjectOutputStream(fos);				
		
								oos.writeObject(fw);	//write four wheeler object to file
								oos.close();
							}
							catch(Exception e)
							{
								System.out.println("Error : " + e);
							}
			
							if(choice2 == 1)
							{
								pv.update4wPrivate();	
								
								try{	
									fos = new FileOutputStream(filename, false);		
									oos = new ObjectOutputStream(fos);				
			
									oos.writeObject(pv);	//write private four wheeler object to file
									oos.close();
								}
		
								catch(Exception e)
								{
									System.out.println("Error : " + e);
								}
							}
							
							else
							{
								cv.update4wCommercial();
								
								try{	
									fos = new FileOutputStream(filename, false);		
									oos = new ObjectOutputStream(fos);				
			
									oos.writeObject(cv);	//write commercial four wheeler object to file
									oos.close();
								}
		
								catch(Exception e)
								{
									System.out.println("Error : " + e);
								}
							}
						}
						System.out.println("File update sucessfull! \n");
					}
					
					else
					{
						System.out.println("File doesn't exist! \n");
					}
					
				break;
			
				case 3:
					System.out.print("Enter filename you want to delete : ");
					filename = sc.next();
					
					f = new File(filename);
					
					if(f.isFile())
					{
						f.delete();
						System.out.println("File delete sucessfull! \n");
					}
					else
					{
						System.out.println("File doesn't exist! \n");
					}
					
				break;
			
				case 4:
					System.out.print("Enter filename you want to show : ");
					filename = sc.next();
					
					f = new File(filename);		//object of file
					FileInputStream fis = null;
					ObjectInputStream ois = null;
					
					Vehicle temp_v;				///reference of vehicle
					TwoWheeler temp_tw = new TwoWheeler();	//object of two v.
					PrivateVehicle temp_pv = new PrivateVehicle();	//object of private four v.
					CommercialVehicle temp_cv = new CommercialVehicle();	//objec of commmercial four v.	
					FourWheeler temp_fw = temp_pv;		//object of four wheeler
					
					try{
					
					fis = new FileInputStream(filename);		
					ois = new ObjectInputStream(fis);
										
					if(f.isFile())
					{
						System.out.println("Press 1 --> Two wheeler : ");
						System.out.println("Press any other number --> Four wheeler : ");
						choice1 = sc.nextByte();
		
						if(choice1 == 1)
						{
							temp_tw = (TwoWheeler)ois.readObject();		//read object from file
							temp_tw.show2w();	//call show fuction
							
						}
						else		
						{
							System.out.println("Press 1 --> Private vehicle : ");
							System.out.println("Press any other number --> Public vehicle : ");
							choice2 = sc.nextByte();
			
							if(choice2 == 1)
							{
								temp_pv = (PrivateVehicle)ois.readObject();	//read object of private four v.
								temp_pv.show4wPrivate();	//call show function
								
							}
			
							else
							{
								temp_cv = (CommercialVehicle)ois.readObject();	//read object of commercial vehicle
								temp_cv.show4wCommercial();	//call show function
								
							}
						}
					}
					}
					
					catch(Exception e)
					{
						System.out.println("Error occured : " + e);
						System.out.println("Enter Appropriate Details!\n");
					}
					
					finally
					{
						try{
						ois.close();
						}
						catch(Exception e)
						{
							System.out.println("Error occured : " + e);
						}
					}
				break;
			}
		}while(option >0 && option<5);
	}
}