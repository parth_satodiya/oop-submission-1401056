					//Assignment 8
					//count elements, copy and rename file

import java.util.Scanner;
import java.io.*;			//import file library

class Count			
{
	int i,j=0;
	double count;
	public Count()		//default constructor
	{
		i=0;
		count=0;
	}

	public void countCharacters(String file)	//function for character count
	{
		try{
			FileInputStream fis = new FileInputStream(file);		//create object os input stream
			for(i=0;i!=-1;)
			{
				i=fis.read();
				if((char)i != ' ')		// space doesn't count in character
				{
					count++;
				}
			}
			fis.close();			//close file
		}
		catch(IOException ex)
		{
		    System.out.println("Error reading to file '"+ file + "'");
			j++;
		   
		}
		if(j==0)
			System.out.println("Total characters are : " + (count-1));		//print total count character
		else
			System.out.println("Characters count unsuccessful!");
	}
	
	public void countWords(String file)
	{
			try{
				FileInputStream fis = new FileInputStream(file);	//create object for input stream
				for(i=0;i!=-1;)
				{
					i = fis.read();
					if(i == ' ' || i =='\n')			//if space and linebreak comes then count word
					{
						count++;
					}
					else
					{
						continue;
					}
				}
			}
			
			catch(Exception e)
			{
				System.out.println("Error reading to file '"+ file + "'");
				j++;
			}
		if(j==0)
			System.out.println("Total words in file are : "+(count+1));		//print total no of word
		else
			System.out.println("Words count unsuccessful!");

	}
	
	public void countLines(String file)			//function for count lines
	{
		try
		{
			FileInputStream fis = new FileInputStream(file);	//create object for input stream
			for(i=0;i!=-1;)
			{
				i = fis.read();
				if(i =='\n')			///if linebreak comes then count one line
				{
					count++;
				}
				else
				{
					continue;
				}
			}
		}
		
		catch(Exception e)
		{
			System.out.println("Error reading to file '"+ file + "'");
			j++;
		}
		if(j==0)
			System.out.println("Total Lines in file are : "+(count+1));
		else
			System.out.println("Lines count unsuccessful!");
		
	}
	
	public void copyFile(String source, String copied)	//function for copy file. Took two input string parameter(source,destination)
	{
		try
		{
			FileInputStream fis = new FileInputStream(source);	//create object for input stream
			FileOutputStream fos = new FileOutputStream(copied);	//create object for output stream
	
			for(i=0; i!=-1;)
			{
				i = fis.read();		//read source file
				fos.write(i);		//write read character in destination file 
			}
			
			fis.close();
			fos.close();
		}
		
		catch(Exception e)
		{
			System.out.println("Error reading file '"+ source + "'");
			j++;
		}
		if(j==0)
			System.out.println("File Copied!");
		else
			System.out.println("Copy operation unsuccessful!");
	}
	
	public void rename(String file, String new_name)	//function for rename file
	{
		File o = new File(file);
		File n = new File(new_name);
		if(o.renameTo(n))
			System.out.println("Renamed!");
		else
			System.out.printf("Rename account failed");
	}
}

class CountFile
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		String filex;
		int choice = 0;

		System.out.println("\n_________________________________________\n");
		System.out.println("Press 1 : Count File Characters: ");
		System.out.println("Press 2 : Count File Words: ");
		System.out.println("Press 3 : Count File Lines: ");
		System.out.println("Press 4 : Copy File : ");
		System.out.println("Press 5 : Rename File : ");
		choice = sc.nextInt();


		Count C = new Count();		//create object of Count class

		switch(choice)
		{
			case 1:
			
			System.out.println("Enter File Name : ");	//take indivisual filename for each class
			filex = sc.next();

			C.countCharacters(filex);
			break;
			
			case 2:
			System.out.println("Enter File Name : ");
			filex = sc.next();

			C.countWords(filex);
			break;
			
			case 3:
			System.out.println("Enter File Name : ");
			filex = sc.next();
			
			C.countLines(filex);
			break;
			
			case 4:
			System.out.println("Enter Source File Name : ");
			String source = sc.next();
			
			System.out.println("Enter Copied File Name");
			String copied = sc.next();
			
			C.copyFile(source, copied);
			break;
			
			case 5:
			System.out.println("Enter File name : ");
			filex = sc.next();
			
			System.out.println("Enter new name : ");
			String new_name = sc.next();
			
			C.rename(filex, new_name);
		}
	}
}



