import java.util.Scanner;

class Point
{
	int i;
	int size;
	double [] ele;
	Scanner sc = new Scanner(System.in);
	
	Point()							//constructor
	{
		size = 0;
		ele = new double[0]; 
	}
	
	Point(int s)						//peramiterise constructor
	{
		size = s;
		ele = new double[s];
	} 
	
	public Point(Point A)					//copy constructor
	{
		size = A.size;
		ele = new double[size];
		
		for(i=0;i<size;i++)
		{
			ele[i] = A.ele[i];
		}
	}
	
	void enterEle(int d)					//Enter element in points
	{
		for(i=0;i<d;i++)
		{
			System.out.println("Enter " + (i+1) + " element");
			ele[i] = sc.nextDouble();
		}
	}
	
	boolean check(Point A)					//check for same points (boolean type)
	{
		if(size != A.size)				//first compare size
		{
			return false;
			
		}
		
		for(i=0;i<size;i++)
		{
			if(ele[i] != A.ele[i])			//compare elements if size is matched
			{
				return false;
			}
			
			else if(i == (size-1))
			{
				return true;
			}
			
			else
			{
				continue;
			}
		}
		return true;
	}
	
	void showPoint()					//function for watch points
	{
		for(i=0;i<size;i++)
		{
			if(i==0)
			{
				System.out.print("(" + ele[i] + ", ");
			}
			
			else if(i>0 && i<(size-1))
			{
				System.out.print(ele[i] + ", ");
			}
			
			else
			{
				System.out.print(ele[i] + ")");
				System.out.println("\n");
			}
		}
	}
}

class PointCheck
{
	public static void main(String argv[])
	{
		int n,i;
		int dim,choice;
		int first,second;
		boolean c;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("How many points you want to enter ? : ");	//take howmany points
		n = sc.nextInt();
		
		Point[] P = new Point[n];
		
		for(i=0;i<n;i++)
		{
			System.out.println("Enter dimension for " + (i+1) + " point");	//take point's dimension
			dim = sc.nextInt();
			
			P[i] = new Point(dim);						//perameterise constructor
			P[i].enterEle(dim);	
			
			System.out.print("\n");		
		}
		
		do{
		System.out.print("\n");
		System.out.println("Press 0 : Exit : ");
		System.out.println("Press 1 : Compare Points : ");
		System.out.println("Press 2 : Copy point : ");
		System.out.println("Press 3 : Shoe all Points : ");
		choice = sc.nextInt();
		
			switch(choice)
			{
			case 1:
		
				if(n < 2)
				{
					System.out.println("Enter minimum 2 points.!");
				}
			
				else
				{
					System.out.println("Enter first point's number : ");
					first = sc.nextInt();
				
					System.out.println("Enter second point's number : ");
					second = sc.nextInt();
					
					if(first<1 || second<1 || first>n || second>n)
					{
						System.out.println("Enter valid point number.! \n");
					}
				
					else
					{
						c = P[(first-1)].check( P[(second - 1)] );	//check function called
				
						if(c)
						{
							System.out.println("\nPoint is matched.! \n");
						}
				
						else
						{
							System.out.println("\nPoints doesn't matched.! \n");
						}
					}
				} 
				break;
			
			case 2:
				
				if(n < 2)
				{
					System.out.println("Enter minimum 2 points.!");
				}
			
				else
				{
					System.out.println("Enter first point's number : ");
					first = sc.nextInt();
				
					System.out.println("Enter second point's number : ");
					second = sc.nextInt();
					
					if(first<1 || second<1 || first>n || second>n)
					{
						System.out.println("Enter valid point number.! \n");
					}
					
					else
					{
						P[(second-1)] = new Point( P[(first-1)] );		//copy constructor called
						System.out.print("Copied.! \n");					
					}
				} 
				break;
				
			case 3:
				for(i=0;i<n;i++)					//show all points
				{
					System.out.println((i+1) + " point");
					P[i].showPoint();
				}
			}
		
		
		}while(choice!=0);
	}
}
