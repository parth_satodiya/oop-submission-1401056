import java.util.Scanner;

class Account
{
	Scanner S=new Scanner(System.in);
	
	public String name,address,branch_name;
	public int mobile,rupee,account_no;
	
	
	public void addItem()									//Member Function for add Account Details.
	{
		System.out.print("Enter Your Name 				: ");
		name=S.next();
		System.out.print("\n");

		System.out.print("Enter Your Address 				: ");
		address=S.next();
		System.out.print("\n");

		System.out.print("Enter Your Mobile No. 				: ");
		mobile=S.nextInt();
		System.out.print("\n");

		System.out.print("Enter Branch Name 				: ");
		branch_name=S.next();
		System.out.print("\n");

		System.out.print("Enter How many Rupee put in Your Account 	: ");
		rupee=S.nextInt();
		System.out.print("\n");
		
		System.out.print("Enter Account No. 				: ");
		account_no=S.nextInt();
		System.out.print("\n");
		System.out.print("\n");
	}
	
	public void deleteItem()										//Member Function for delete Account.
	{
		name=null;
		address=null;
		branch_name=null;
		mobile=0;
		rupee=0;
		account_no=0;
	}
	
	public void showItem()											//Member Function for Show Account Details.
	{
		System.out.println("Your Name 			: " + name);

		System.out.println("Your Address 			: " + address);

		System.out.println("Your Mobile No. 		: " + mobile);

		System.out.println("Branch Name 			: " + branch_name);

		System.out.println("Total balance in Your Account 	: " + rupee);
		
		System.out.println("Your Account No. is 		: " + account_no);
		
		System.out.println("\n\n");
	}
	
	public void copyObject(Account C)							//Member Function for copy Account Details.
	{
		this.name=C.name;
		this.address=C.address;
		this.mobile=C.mobile;
		this.branch_name=C.branch_name;
		this.rupee=C.rupee;
		this.account_no=C.account_no;
	}
}

class List														//Class include main class
{
		public static void main(String argv[])
		{	
			Scanner S=new Scanner(System.in);
			
			int o,s,d,n,j,l,op,k=-1;
			int i=0;
			
			System.out.print("How many account do you want to enter  ");
			n=S.nextInt();
			System.out.print("\n");
			
			Account[] A=new Account[n];							//create array of objects..
			l=n;
			
			do												//do-while loop for repeat program.
			{					
				System.out.println("Press 1 --> Add account 					:");
				System.out.println("Press 2 --> Delete account 					:");
				System.out.println("Press 3 --> Show particular account 				:");
				System.out.println("Press 4 --> Check duplicate account				:");
				System.out.println("Press 5 --> Print all accounts 					:");
				System.out.println("Press 6 --> Put Accounts in ascending / descending order 	:");
				System.out.println("Press 7 --> Max. / Min. 					:");
				System.out.println("Press 8 --> Print all Account reverse 				: ");
				System.out.println("Press 9 --> Credit / Debit in your Account 			:");
				System.out.println("Press 0 --> Exit 						:");
				
				o=S.nextInt();
				System.out.println("\n");
				
				switch(o)
				{
					case 1:																	//Add account details
					if(i==n && k==-1)						//print this message while account object array is full.
					{
						System.out.println("No other account Accepted!");
						System.out.print("\n");
						break;
					}
					
					else if(k!=-1)
					{
						A[k]=new Account();
						A[k].addItem();
						k=-1;
						break;
					}
				
					else
					{
						A[i]=new Account();
						A[i].addItem();
						
						i++;
						break;
					}
					
					
					case 2:																	//For  delete account
					System.out.print("Enter Account no. which you want to delete : ");
					d=S.nextInt();
					System.out.print("\n");
					
					for(j=0;j<n;j++)
					{
						if(d==A[j].account_no)												//Search account.
						{
							System.out.println("Account number matched!");
							A[j].deleteItem();
							System.out.print("\n");
							
							System.out.println("Account is deleted..!");
							k=j;
							System.out.print("\n");
							System.out.print("\n");
							break;
						}
						
						else if(j==n-1 && d!=A[n-1].account_no)
						{
							System.out.println("Account number doesn't match!");
							System.out.println("\n");
							break;
						}
					}
					break;
					
					case 3:																	//Show particular account details
					System.out.print("Enter Account no. which you want to watch : ");
					d=S.nextInt();
					System.out.print("\n");
					
					for(j=0;j<n;j++)
					{
						if(d==A[j].account_no)												//Search account.
						{
							System.out.println("Account number matched!");
							System.out.print("\n");
							
							A[j].showItem();
							
							System.out.print("\n");
					
							break;
						}
						
						else if(j==n-1 && d!=A[n-1].account_no)
						{
							System.out.println("Account number doesn't match!");
							System.out.print("\n");
							break;
						}
					}
					break;
					
					case 4:								//delete account if the same ac_no. of account created.
					for(j=0;j<n;j++)
					{
						for(s=j+1;s<n;s++)
						{
							if(A[j].account_no==A[s].account_no)					//Search duplicate account.
							{
								System.out.println("Account number matched!");
								A[s].deleteItem();
								System.out.print("\n");
								
								System.out.println("Account is deleted..!");
								k=s;
								System.out.print("\n");
								System.out.print("\n");
								break;
							}
							
							else if(s==n-1 && (A[s].account_no!=A[n-1].account_no))
							{
								System.out.println("There is no duplicate Account.");
								break;
							}
						}
					}
					break;
					
					case 5:											//print all accounts.
					for(j=0;j<n;j++)
					{
						if(A[j].name!=null)
						{
							A[j].showItem();
							System.out.println("\n");
						}
						else if(j==n-1 && A[j].name!=null)
						{
							break;						
						}
					}
					break;
					
					case 6:													//sort all accounts by account_no wise.
					
						System.out.println("Press 1 --> Sort Accounts as ascending order(by Account number) : ");
						System.out.println("Press 2 --> Sort Accounts as descending order (by Account number) : ");
						op=S.nextInt();
					
						if(op==1)
						{	
							for(j=0;j<n-1;j++)
							{
								for(s=j+1;s<n;s++)
								{
									if(A[j].account_no>A[s].account_no)
									{
										String tempName=A[j].name;
										String tempAdd=A[j].address;
										String tempBranch=A[j].branch_name;
										int tempMob=A[j].mobile;
										int tempRup=A[j].rupee;
										int tempAc=A[j].account_no;
										
										A[j].name=A[s].name;
										A[j].address=A[s].address;
										A[j].branch_name=A[s].branch_name;
										A[j].mobile=A[s].mobile;
										A[j].rupee=A[s].rupee;
										A[j].account_no=A[s].account_no;
										
										A[s].name=tempName;
										A[s].address=tempAdd;
										A[s].branch_name=tempBranch;
										A[s].mobile=tempMob;
										A[s].rupee=tempRup;
										A[s].account_no=tempAc;
									}
								}
							}
							System.out.println("Accounts are sorted in ascending order...");
							System.out.println("\n");
					
							break;
						}
					
						else if(op==2)
						{
							for(j=0;j<n-1;j++)
							{	
								for(s=j+1;s<n;s++)
								{
									if(A[j].account_no<A[s].account_no)
									{
										String tempName=A[j].name;
										String tempAdd=A[j].address;
										String tempBranch=A[j].branch_name;
										int tempMob=A[j].mobile;
										int tempRup=A[j].rupee;
										int tempAc=A[j].account_no;
										
										A[j].name=A[s].name;
										A[j].address=A[s].address;
										A[j].branch_name=A[s].branch_name;
										A[j].mobile=A[s].mobile;
										A[j].rupee=A[s].rupee;
										A[j].account_no=A[s].account_no;
										
										A[s].name=tempName;
										A[s].address=tempAdd;
										A[s].branch_name=tempBranch;
										A[s].mobile=tempMob;
										A[s].rupee=tempRup;
										A[s].account_no=tempAc;
									}
								}
							}
						}
							System.out.println("Accounts are sorted in descending order...");
							System.out.println("\n");
					break;
					
					case 7:												//show max./ min. account details by account_no wise.
					Account max_min=new Account();
					
					max_min.copyObject(A[0]);
					
					System.out.println("Press 1 --> Print Max. Account (by Account number) : ");
					System.out.println("Press 2 --> Print Min. Account (by Account number) : ");
					op=S.nextInt();
					
					if(op==1)
					{						
						for(j=0;j<n;j++)
						{
							if(max_min.account_no<A[j].account_no)
							{
								max_min.copyObject(A[j]);
							}
						}
						max_min.showItem();					
					}
					
					else if(op==2)
					{
						for(j=0;j<n;j++)
						{
							if(max_min.account_no>A[j].account_no)
							{
								max_min.copyObject(A[j]);
							}
						}
						max_min.showItem();	
					}
					break;
					
					case 8:										//Print all account in reverse order
					for(j=n-1;j>=0;j--)
					{
						A[j].showItem();
					}
					break;
					
					case 9:												//For Enter debit/credit details.
					System.out.println("Press 1 --> For Debit : ");
					System.out.println("Press 2 --> For Credit : ");
					op=S.nextInt();
					System.out.println("\n");
					
					System.out.print("Enter Account No. : ");
					d=S.nextInt();
					System.out.println("\n");
					
					for(j=0;j<n;j++)
					{
						if(d==A[j].account_no && op==1)
						{
							System.out.print("How many rupee you debited from your account : ");
							s=S.nextInt();
							System.out.println("\n");
							
							if(A[j].rupee>=s)
							{
								A[j].rupee=A[j].rupee - s;
								System.out.println("Update Successfully.!");
								System.out.println("\n");
								break;
							}
							
							else if(A[j].rupee<s && op==1)
							{
								System.out.println("No sufficient balance in your Account..!");
								System.out.println("\n");
								break;
							}
						}
						
						if(d==A[j].account_no && op==2)
						{
							System.out.println("How many rupee you credited in your account : ");
							s=S.nextInt();
							
							A[j].rupee = A[j].rupee + s;
							System.out.println("Update Successfully.!");
							System.out.println("\n");
							break;
						}
					}
					break;
					
				}
			}while(o!=0);
		}
}
