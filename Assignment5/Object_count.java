import java.util.*;

class Item
{
	static int s=0;			// Took static variable to see howmany items you entered
	int k;
	String n;
	
	
	Item()
	{
		s++;			//Increment of static variable when constructor will called
	}
	
	void putProIdAndName(int i,String name)		//funnction to enter product id and name
	{
		k=i;
		n=name;
	}
	
	static int getStatic()
	{
		return s;		//static function which return static variable
	}
	
	int getProId()
	{
		return k;		//return Product Id
	}
	
	String getProName()		//Return product name
	{
		return n;
	}
	
}

//______________________________________________________________________________________________________________

class Object_count			//new class included main function
{
	public static void main(String args[])
	{
		Scanner S=new Scanner(System.in);
	
		int h,a,i;
		String b;
	
		System.out.println("How many Items You want to enter : ");
		h=S.nextInt();
		
		Item[] I=new Item[h];				//Create array of objects

		System.out.println("-----------------------------------------------------------");		
		System.out.println("No Of Item you Entered : "+ Item.getStatic() + "\n\n");
		
		for(i=0;i<h;i++)
		{
		
			I[i]=new Item();		//Every time call constructor while creating new object..
		
	
			System.out.println("Enter Product no.");
			a=S.nextInt();
	
			System.out.println("Enter Product Name : ");
			b=S.next();
	
			I[i].putProIdAndName(a,b);
	
			System.out.println("\nYour Product NO. is :" + I[i].getProId() +"\nProduct Name is :"+I[i].getProName() + "\n");
		
			System.out.println("No Of Item you Entered : "+ Item.getStatic() + "\n");
			
			System.out.println("-----------------------------------------------------");
		
		}
	}
}
