import java.util.Scanner;

abstract class Account					//abstract account class
{
	private String account_no;
	protected float balance;
	
	public Account(String ac_no, float bal)		//Parent class's constrator
	{
		account_no = ac_no;
		balance = bal;
	}
	
	public void credit(float r)			//common credit function 
	{
		balance += r;
	}
	
	public float getBalance()			//common get balance function
	{
		return balance;
	
	}
	
	public String getAccountNo()			//common get account number function
	{
		return account_no;
	}
	
	public abstract void withdraw(float withdraw);	//abstract function withdraw because it is necessary for both child class 
}

class Savings extends Account				//child class of Account class		(for savings account)
{
	Scanner sc = new Scanner(System.in);
	
	private float min_balance = 500;					//must remain 500 rupees every time in your account
	private float interest_rate, interest, total_interest=0, temp1=1, temp2;
	private int i, current_month, current_year, month, year, total_month = 3;
	private int quarter;
		
	public Savings(float interest, String ac_no, float bal)		//constructor for saving account
	{
		super(ac_no, bal);					//call parent class constructor
		interest_rate = interest;
	}	
		
	public void setCurrentDate(int cm, int cy)			//function for set today's date
	{
		current_month = cm; 		
		current_year = cy;
	}
	
	public boolean setAccountDate()					//Enter date when you created account
	{
		System.out.println("Enter account month : ");	
		month = sc.nextInt(); 	
		
		System.out.println("Enter account year : ");	
		year = sc.nextInt();
		
		if(month <= 0 || year <= 0 || month > 12)		//check valid date 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void interest()						//function for calculate interest quataraly
	{
		if(year == current_year && month>current_month)		//account date is less then today's date
		{
			System.out.println("Account date is Incorrect!");
			return;
		}
		else if(year>current_year)				//account date is less then today's date
		{
			System.out.println("Account date is Incorrect!");
			return;
		}
		
		else if(total_month < 3)				//total_month check the difference between today's and account's date
		{
			System.out.println("You already calculate interest for this account!");
			return;
		}
		
		else
		{
			total_month = ((12 *( current_year - year)) + current_month) - month;		
			
			/* calculate total month difference between today's date and account's date*/
													
			quarter = (total_month/3);		//calculate quarter
		
			for(i=1; i<=quarter; i++)
			{
				interest = (balance * interest_rate * ((float)1/4)) / 100;      //calculate simple interest for each quarter
				System.out.println(i + " quarter interest is : " + interest);
				balance = balance + interest;
				total_interest = total_interest + interest;
			}
					
			System.out.println("Your interest is : "+ total_interest);
			System.out.println("Your final balance is : " + balance+ "\n");
			
			total_month = total_month - (3 * quarter);
			
			System.out.println("Your next interest will applied after "+ (3 - total_month) +" month/s \n");
		}
	}
	
	public void setMinBalance(float set_minimum_balance)		//set min balance which is min for saving account. less then this is not allowed
	{
		min_balance = set_minimum_balance;		
	}
	
	public float showMinBalance()					//show main balance
	{
		return min_balance;
	}
	
	public float showInterestRate()					//show interest rate for saving account
	{
		return interest_rate;
	}
	
	public void setInterestRate(float set_rate)			//set interest rate 
	{
		interest_rate = set_rate;		
	}
	
	public void showAccount()					//show saving account
	{
		System.out.println("Account No. : " + getAccountNo());
		System.out.println("Min balance for saving accounts : " + showMinBalance());
		System.out.println("Balance : " + getBalance());
		System.out.println("Interest rate : "+ showInterestRate());
	}
	
	public void withdraw(float withdraw)				//withdraw function for saving account
	{
		if(withdraw > (balance-min_balance) )
		{
			System.out.println("Insufficient balance ( balance must remain greater then 500 for all time. )");
		}
		
		else
		{
			balance -= withdraw;
		}
	}
}

class Current extends Account				//child class of account class    (for currennt account)
{
	private float[] overdraft_limit = new float[]{500, 5000, 50000, 500000, 5000000, 10000000};

	float income;
	
	Current(float in, String ac_no, float bal)		//constructor for current
	{
		super(ac_no, bal);				//parent class's constructor
		income = in;
	}
	
	public float showIncome()				//show income
	{
		return income;
	}
	
	public void showAccount()				//show current account
	{
		System.out.println("Account No. : "+getAccountNo());
		System.out.println("Account Balance : "+getBalance());
		System.out.print("Account Overdraft Limit : ");
		showOverdraftLimit();
	}
	
	public void setOverdraftLimit(float over_limit)		//set overdraft limit for your current account (income wise)
	{
		if(income < 6000)
		{
			overdraft_limit[0] = over_limit;
		}	
		
		if(income < 60000)
		{
			overdraft_limit[1] = over_limit;
		}	
		
		if(income < 800000)
		{
			overdraft_limit[2] = over_limit;
		}	
		
		if(income < 2000000)
		{
			overdraft_limit[3] = over_limit;
		}	
		
		if(income < 30000000)
		{
			overdraft_limit[4] = over_limit;
		}	
		
		if(income > 30000000)
		{
			overdraft_limit[5] = over_limit;
		}	
	}
	
	public void showOverdraftLimit()			//show overdraft limit for current account (income wise)
	{
		if(income < 6000)
		{
			System.out.println("Overdraft limit for this account is 500"); 
		}
		
		else if(income < 60000)
		{
			System.out.println("Overdraft limit for this account is 5000"); 
		}
		
		else if(income < 800000)
		{
			System.out.println("Overdraft limit for this account is 50000"); 
		}
		
		else if(income < 2000000)
		{
			System.out.println("Overdraft limit for this account is 500000"); 
		}
		
		else if(income < 30000000)
		{
			System.out.println("Overdraft limit for this account is 5000000"); 
		}
		
		else if(income > 30000000)
		{
			System.out.println("Overdraft limit for this account is 10000000"); 
		}
	}		
	
	
	public void withdraw(float withdraw)			//withdraw function for current account 
	{
		if(income < 6000)
		{
			if(withdraw > ( balance + overdraft_limit[0]))
			System.out.println(" Insufficient balance! (Yor cross the overdraft limit "+ overdraft_limit[0] +")");
						
			else
			balance -= withdraw; 
		}
		
		else if(income < 60000)
		{
			if(withdraw > ( balance + overdraft_limit[1]))
			System.out.println(" Insufficient balance! (Yor cross the overdraft limit 5000 " +overdraft_limit[1] +")");
			
			else
			balance -= withdraw; 
		}
		
		else if(income < 800000)
		{
			if(withdraw > ( balance + overdraft_limit[2]))
			System.out.println(" Insufficient balance! (Yor cross the overdraft limit 50000 " + overdraft_limit[2]+ ")");
			
			else
			balance -= withdraw; 
		}
		
		else if(income < 2000000)
		{
		
		
			if(withdraw > ( balance + overdraft_limit[3]))
			System.out.println(" Insufficient balance! (Yor cross the overdraft limit 500000 " + overdraft_limit[3]+ ")");
			
			else
			balance -= withdraw; 
		}
		
		else if(income < 30000000)
		{
			if(withdraw > ( balance + overdraft_limit[4]))
			System.out.println(" Insufficient balance! (Yor cross the overdraft limit 5000000 " + overdraft_limit[4]+ ")");
			
			else
			balance -= withdraw; 
		}
		
		else if(income > 30000000)
		{
			if(withdraw > ( balance + overdraft_limit[5]))
			System.out.println(" Insufficient balance! (Yor cross the overdraft limit 10000000 " +overdraft_limit[5] + ")");
			
			else
			balance -= withdraw; 
		}
	}		
}

class Banking						//main banking class
{
	public static void main(String argv[])
	{
		Scanner sc = new Scanner(System.in);
		
		int i, sj=0, cj=0, s, c, current_month, current_year;
		byte option, o1, o2;
		Account Ac;
		float rate;
		String ac_no;
		float in, min_balance, bal, limit, credit, withdraw;		
				
		System.out.println("Howmany Savings accounts you want to create? : ");
		s = sc.nextInt();
		Savings[] S = new Savings[s];					//saving object array
		
		
		System.out.println("Howmany Current accounts you want to create? : ");
		c = sc.nextInt();
		Current[] C = new Current[c];					//current object array
		
		do{
		System.out.println("______________________________________________________________");		
		System.out.println("Press 1 : Add savings account's details : ");
		System.out.println("Press 2 : Add current account details : ");
		System.out.println("Press 3 : Set Minimum balance for savings account : ");
		System.out.println("Press 4 : Set Interest rate for savings account : ");
		System.out.println("Press 5 : Set Overdraft limit for current account : ");
		System.out.println("Press 6 : Credit/Withdraw : ");
		System.out.println("Press 7 : View accounts : ");
		System.out.println("Press 8 : View interest with final balance : ");
		System.out.println("Press 0 : Exit : ");
		option = sc.nextByte();
		
		switch(option)		
		{
			case 1:
			if(s==0)
			{
				break;
			}
			
			else
			{
				for(i=0; i<s; i++)
				{	
					System.out.println("Enter account no. : ");
					ac_no = sc.next();
					
					System.out.println("Enter savings interest rate : ");
					rate = sc.nextFloat();		
					
					if(rate < 0.1 || rate > 20)
					{
						System.out.println("Entered incorrect details!");
						break;
					}
						
					do{
						System.out.println("Enter inialtial balance (must be greaterthen 1000) : ");
						bal = sc.nextFloat();
					}while(bal < 1000);
	
					System.out.println("Savings account created!");
	
					S[i] = new Savings(rate, ac_no, bal);			//create saving class's object
					
					if(S[i].setAccountDate())				//call boolean function fro set account date
					{
						System.out.println("Entered incorrect details!");
						break;
					}
					sj++;
					System.out.println("__________________________________________________\n");

				}
					System.out.println("Enter current month : ");	
					current_month = sc.nextInt(); 	
		
					System.out.println("Enter current year : ");	
					current_year = sc.nextInt();
					
					if(current_month < 1 || current_year < 1 || current_month > 12)	//check validity for date
					{
						System.out.println("Entered incorrect details!");
						break;
					}
					for(i=0;i<s;i++)				//set today's date for all saving account's object
					{
						S[i].setCurrentDate(current_month, current_year);					
					}
					
				break;
			}
			
			case 2:
			if(c==0)
			{
				break;
			}
			
			else
			{
				for(i=0; i<c; i++)
				{
					System.out.println("Enter your income : ");
					in = sc.nextFloat();
	
					System.out.println("Enter account no. : ");
					ac_no = sc.next();
	
					System.out.println("Enter initial balance : ");
					bal = sc.nextFloat();
					
					if(in < 1 ||  bal < 1)
					{
						System.out.println("Entered incorrect details!");
						break;
					}
	
					System.out.println("Current account created!");
	
					C[i] = new Current(in, ac_no, bal);		//create current class's object
					cj++;
					System.out.println("__________________________________________________\n");			
				}
			}
			break;
			
			case 3:
			
			if(sj == 0)		//check whether account details entered or not
			{
				System.out.println("No account crated!");
				break;
			}
			else
			{
				System.out.println("Enter Savings Account no.");
				ac_no = sc.next();
			
				for(i=0; i<s; i++)
				{
					if(ac_no == S[i].getAccountNo())
					{
						System.out.println("Current minimum balance is : " + S[i].showMinBalance());
			
						System.out.println("Enter minimun balance : ");
						min_balance = sc.nextFloat();
			
						S[i].setMinBalance(min_balance);	
						break;				
					}
				}
				break;
			}
			
			case 4:
			
			if(sj == 0)		//check whether account details entered or not
			{
				System.out.println("No account crated!");
				break;
			}
			
			else{
				System.out.println("Enter Savings Account no.");
				ac_no = sc.next();
			
				for(i=0; i<s; i++)
				{
					if(ac_no.equals(S[i].getAccountNo()))
					{
						System.out.println("Current Interest rate is : " + S[i].showInterestRate());
			
						System.out.println("Enter new interest rate : ");
						rate = sc.nextFloat();
			
						S[i].setInterestRate(rate);		//set interest rate
						break;				
					}
				}
				break;
			}
			
			case 5:
			
			if(cj == 0)		//check whether account details entered or not
			{
				System.out.println("No account crated!");
				break;
			}
			
			else
			{
				System.out.println("Enter current Account no.");
				ac_no = sc.next();
			
			
				for(i=0; i<c; i++)
				{
					if(ac_no.equals(C[i].getAccountNo()))
					{
						C[i].showOverdraftLimit();
			
						System.out.println("Enter new Overdraft limit for this account : ");
						limit = sc.nextFloat();
			
						C[i].setOverdraftLimit(limit);	
						break;			
					}
				}
			}
			break;
			
			case 6:
			
			System.out.println("Press 1: Savings account");
			System.out.println("Press 2: Current account");
			o1 = sc.nextByte();
			
				switch(o1)
				{
					case 1:	
					if(sj == 0)		//check whether account details entered or not
					{
						System.out.println("No account crated!");
						break;
					}
					else
					{
						System.out.println("Enter savings Account no.");
						ac_no = sc.next();
			
						for(i=0; i<s; i++)
						{
							if(ac_no.equals(S[i].getAccountNo()))	//check account no equal or not
							{
								System.out.println("Press 1: Credit");
								System.out.println("Press 2: withdraw");
								o2 = sc.nextByte();
								 
								if(o2 == 1)
								{
									System.out.println("Enter amount for credit : ");
									credit = sc.nextFloat();
							
									S[i].credit(credit);	//call credit function
									break;	
								}
						
								else
								{
									System.out.println("Enter amount for withdraw : ");
									withdraw = sc.nextFloat();
							
									S[i].withdraw(withdraw);//call saving class's withdraw function
									break;
								}
								
							}
							else
							{
								continue;
							}
						}
					}
					break;
					
					
					case 2:
					
						if(cj == 0)		//check whether account details entered or not
						{
							System.out.println("No account crated!");
							break;
						}
						else
						{
							System.out.println("Enter current Account no.");
							ac_no = sc.next();
			
							for(i=0; i<c; i++)
							{
								if(ac_no.equals(C[i].getAccountNo()))
								{
									System.out.println("Press 1: Credit");
									System.out.println("Press 2: withdraw");
									o2 = sc.nextByte();
									 
									if(o2 == 1)
									{
										System.out.println("Enter amount for credit : ");
										credit = sc.nextFloat();
							
										C[i].credit(credit);
										break;	
									}
						
									else
									{
										System.out.println("Enter amount for withdraw : ");
										withdraw = sc.nextFloat();	//call current class's withdraw function
							
										C[i].withdraw(withdraw);
										break;	
									}
								
								}
					
								else
								{
									continue;
								}
							}
						}
					break;
				}
				break;
				
			case 7:
			
			System.out.println("Press 1: Savings account");
			System.out.println("Press 2: Current account");
			o1 = sc.nextByte();
		
			switch(o1)
			{
				case 1:	
					if(sj == 0)		//check whether account details entered or not
					{
						System.out.println("No Saving account found!");
						break;
					}
					else
					{
						System.out.println("\nSaving accounts");	
						for(i=0;i<s;i++)
						{
							S[i].showAccount();
							System.out.println("______________________________________________________________");		
						}
					}
					break;
			
				case 2:
					if(cj == 0)		//check whether account details entered or not
					{
						System.out.println("No current account found!");
						break;
					}
					else
					{
						System.out.println("\nCurrent accounts");	
						for(i=0;i<c;i++)
						{
							C[i].showAccount();
							System.out.println("______________________________________________________________");		
						}
					}
					break;	
			}
			break;
			
			case 8:
					if(sj == 0)
					{
						System.out.println("No account crated!");
						break;
					}
					else
					{
						System.out.println("Enter saving Account no.");
						ac_no = sc.next();
	
						for(i=0; i<s; i++)
						{
							if(ac_no.equals(S[i].getAccountNo()))
							{
								S[i].interest();		//calculate interest for saving account
								break;
							}
			
							else
							{
								continue;
							}
						}
					}
					break;
					
		}
		}while(option != 0);
	}
}
